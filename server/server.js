var express = require('express');
var webpack = require('webpack');
var bodyParser = require('body-parser');
var WebpackDevServer = require('webpack-dev-server');
var webpackConfig = require('./webpack-dev.config');
var config = require('./config.json');
var path = require('path');
var root = path.join(__dirname, config.root);
var nodeModules = path.join(__dirname, '../node_modules');
var https = require('http');
var fs = require('fs');


const env = process.argv[2];

//if (env === "dev") {
    var server = new WebpackDevServer(webpack(webpackConfig), {
        contentBase: root,
        publicPath: webpackConfig.output.publicPath,
        hot: true,
        historyApiFallback: true,
		disableHostCheck : true,
    });

    var app = server.app;

    // Serve nodes modules (to be able to include bootstrap.css).
    server.app.use(express.static(nodeModules));

    // Add body-parser
    server.app.use(bodyParser.json({limit: '50mb'}));
    server.app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));


    require('./data-api')(app);

    app.get("*", function (req, res) {
        res.sendFile(__dirname + '/index.html')
    });

    server.listen(config.port, 'localhost', function (err, result) {
        if (err) {
            console.log(err);
        }

        console.log('Server listening on : http(s)://localhost:' + config.port);
    });


/*} else if (env === "prod") {
    const app = express();


    app.use('/static', express.static(path.join(__dirname, '../dist/')));
    require('./data-api')(app);
    app.get("*", function (req, res) {
        res.sendFile(__dirname + '/index.html')
    });
    const httpServer = https.createServer(app);
    //const httpServer = https.createServer(credentials, app);

    httpServer.listen(config.port);

}*/