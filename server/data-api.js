var fs = require('fs');
var path = require('path');

var exportLanguage = path.join(__dirname, '../client/assets/text/languages.json');


module.exports = function (app) {
    app.get('/api/exportLanguage', function (req, res) {
       fs.readFile(exportLanguage, 'utf8', function (err, data) {
           if (err) {
               console.error(err);
               process.exit(1);
           }
           res.writeHead(200, {'Content-Type': 'text/html'});
		   var returnVal = JSON.parse(data);
           res.end(JSON.stringify(returnVal[0]), "utf-8");
       });
   });
};
