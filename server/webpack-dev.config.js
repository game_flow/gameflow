const path = require('path');
const webpack = require('webpack');
const config = require('./config.json');
const root = path.join(__dirname, config.root);
const port = config.port;

module.exports = {
    devtool: 'source-map',

    entry: [
        'webpack-dev-server/client?http://localhost:' + port,
        'webpack/hot/only-dev-server',
        path.join(root, '/js/app')
    ],

    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],

    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: [
                    {
                        loader: 'react-hot-loader'
                    },
                    {
                        loader: 'babel-loader'
                    }
                ],
                include: path.join(root, '/js')
            },
            {
                test: /\.css$/,
                exclude: /\.useable\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"

                    }
                ]
            },
            {
                test: /\.useable\.css$/,
                use: [
                    {
                        loader: "style-loader/useable"
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            {
                test: /\.png$/,
                use: [
                    {
                        loader: "url-loader?limit=100000"
                    }
                ]
            },
            {
                test: /\.(jpg|ico)$/,
                use: [
                    {
                        loader: "file-loader"
                    }
                ]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
                    }
                ]
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
                    }
                ]
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader'
                }]
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
                    }
                ]
            }
        ]
    }
};
