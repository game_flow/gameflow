const path = require('path');
const webpack = require('webpack');
var config = require('./config.json');
var root = path.join(__dirname, config.root);

module.exports = {
    devtool: 'cheap-module-source-map',

    entry: [
        path.join(root, '/js/app')
    ],

    output: {
        path: path.join(__dirname, '../dist/'),
        filename: 'bundle.js',
        publicPath:  '/static/'
    },

    plugins: [
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 15
        }),
        new webpack.optimize.MinChunkSizePlugin({
            minChunkSize: 10000
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            include: path.join(root, '/'),
            sourceMap: false,
            compress: {
                warnings: false,
                screw_ie8: true
            },
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
             },
            comments: false
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.DefinePlugin({
            'process.env': {
            'NODE_ENV': JSON.stringify('production')
        }
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],
    
    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: [
                    {
                        loader: 'react-hot-loader'
                    },
                    {
                        loader: 'babel-loader'
                    }
                ],
                include: path.join(root, '/js')
            },
            {
                test: /\.css$/,
                exclude: /\.useable\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /\.useable\.css$/,
                use: [
                    {
                        loader: "style-loader/useable"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|ico|png)$/,
                use: [
                    {
                        loader: "file-loader"
                    }
                ]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options:{
                            limit:10000,
                            mimetype:'application/font-woff'
                        }
                    }
                ]
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options:{
                            limit:10000,
                            mimetype:'application/octet-stream'
                        }
                    }
                ]
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader'
                }]
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options:{
                            limit:10000,
                            mimetype:'image/svg+xml'
                        }
                    }
                ]
            }
        ]
    }
};
