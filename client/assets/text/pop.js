var language = {};
language.EN = {
    "thisFilLanguage": "EN",
	
	// Header
    "headerTitle": 'Navigation banner',
	
	//Footer
	"GameFlow-Adresse": "GameFlow, SARL with capital of 10 000€ 2 place de Gordes 38000 Grenoble",
	"GameFlow-LegalInfo": "SIRET 818.185.852.00014   intracom VAT number FR 19 818185852",
	"GameFlow-Copyright": "Copyright 2017 GameFlow All right reserved",
	
	// Chimere
	"chimere-logo" : 'http://files.gandi.ws/gandi52401/image/logo.jpg',
	"chimere-date" : 'Released in France 25th june 2016',
	"chimere-catchphrase" : 'Creating marvelous creatures for everybody',
	"chimere-description" : ' this is a placeholder ',
	"chimere-ludochrono" : 'https://www.youtube.com/embed/gy-tG5bu7hQ',
	"chimere-tttv" : 'http://www.trictrac.tv/video/iframe/Chimere_de_l_explication',
	"chimere-rules" : 'http://files.gandi.ws/gandi52401/file/chim_rules_v01.pdf',
	"chimere-LudoVox" : 'Understand the game in only 5 min with  :',
	"chimere-downladRules": 'Click here to download English rules',
	"chimere-downladRulesTitle": 'Game Rules',
	"chimere-Trictrac" : 'Game on TricTrac',
	"chimere-BGG" : 'Game on BGG',
	"chimere-GameInfos": '2 to 5 players, for 20 - 30 minutes, and 5 min to explain the rules ! Price in France: 25€, 8+ '
	};
	
language.FR = {
    "thisFilLanguage": "FR",
	
	// Header
    "headerTitle": 'bandeau de navigation',
	
	//Footer
	"GameFlow-Adresse": "GameFlow, SARL au capital de 10 000€ 2 place de Gordes 38000 Grenoble",
	"GameFlow-LegalInfo": "SIRET 818.185.852.00014  au RCS de Grenoble  TVA intracommunautaire FR 19 818185852",
	"GameFlow-Copyright": "Copyright 2017 GameFlow Tout droit réservé",
	
	// Chimere
	"chimere-logo" : 'http://files.gandi.ws/gandi52401/image/logo.jpg',
	"chimere-date" : 'Sortie en France le 25 juin 2016',
	"chimere-catchphrase" : 'Jeu de création de créatures fabuleuses pour petits et grands',
	"chimere-description" : 'Dans Chimère, chaque joueur incarne un magicien à la cour d\'un roi. Ce roi a décidé de doter son royaume d\'une nouvelle mascotte, il recherche donc le magicien le plus doué qui aura la tâche de lui fabriquer sa mascotte.\n Pour ce faire, il organise un grand tournoi de magie avec différents concours. Pour chaque concours, les joueurs devront fabriquer une créature fabuleuse composée de 3 animaux réels. Ces chimères s\'affronteront et détermineront le meilleur magicien.',
	"chimere-ludochrono" : 'https://www.youtube.com/embed/3OGqBK1CNiU',
	"chimere-tttv" : 'http://www.trictrac.tv/video/iframe/Chimere_de_l_explication',
	"chimere-rules" : 'http://files.gandi.ws/gandi52401/file/regles_chimere.pdf',
	"chimere-LudoVox" : 'Comprendre le jeu en 5 min avec la vidéo LudoChrono :',
	"chimere-downladRules": 'Cliquer ici pour télécharger les règles en français',
	"chimere-downladRulesTitle": 'Règles du Jeu',
	"chimere-Trictrac" : 'Fiche TricTrac',
	"chimere-BGG" : 'Fiche BGG',
	"chimere-GameInfos": "2 à 5 joueurs,<br> Durée : 20 - 30 minutes <u>underline</u>, 5 minutes d\'explications de règles ! Prix public : 25€, A partir de 8 ans"
};

module.exports = language;
