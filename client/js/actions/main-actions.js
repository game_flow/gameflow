
export const UPDATE_BANNER_CLASS = 'UPDATE_BANNER_CLASS';
export function updateBannerClass(bannerClass) {
    return (dispatch) => {
        dispatch({type: UPDATE_BANNER_CLASS, bannerClass});
    }
}


export const UPDATE_POSTER = 'UPDATE_POSTER';
export function updatePoster(imagePath, isVisible, item) { //location
    return (dispatch) => {
        dispatch({type: UPDATE_POSTER, imagePath, isVisible, item});//location
    }
}


export const HIDDE_POPU = 'HIDDE_POPU';
export function hiddePopup() {
    return (dispatch) => {
        dispatch({type: HIDDE_POPU});
    }
}
