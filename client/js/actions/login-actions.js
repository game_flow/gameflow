import superagent from 'superagent';
import {apiUrls} from '../appConfig';
import {merge, concat, sortBy} from 'lodash';
import {bindActionCreators} from 'redux';

/*
export const LOAD_LANGUAGE = 'LOAD_LANGUAGE';
export function loadLanguage(language) {
    return (dispatch) => {
        dispatch({type: LOAD_LANGUAGE, language: language});
    }
}
*/
export const LOAD_LANGUAGE = 'LOAD_LANGUAGE';
export function loadLanguage(language) {
   return (dispatch) => {
       superagent.get('/api/exportLanguage')
           .set('Accept', 'text/plain')
           .end((err, res) => {
               if (res.ok) {
                   let parsedLanguage = JSON.parse(res.text);
                   dispatch({type: LOAD_LANGUAGE,language:parsedLanguage[language]});
               } else {
                   console.error(apiUrls.login, res.status, err.toString());
               }
           });
   }
}