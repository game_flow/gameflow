import React from 'react';

export function getLocalizedElement(translatedValue){
  return  React.createElement('span', { dangerouslySetInnerHTML: { __html: translatedValue }})
};

export function getGlobalOffset(el,container) {
    let x = 0, y = 0;
    let containerX = 0, containerY = 0;
    while (el) {
        x += el.offsetLeft;
        y += el.offsetTop;
        el = el.offsetParent
    }
    if(container){
        containerY -= container.scrollTop;
        containerX -= container.scrollLeft;
    }
    return {x: x + containerX, y: y + containerY}
}

export function getCoords(elem) { // crossbrowser version
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;
    let offsetTop = 0;
    let offsetleft = 0;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    if(elem.classList.value.indexOf('bottom') !== -1){
        offsetTop = -elem.firstChild.clientHeight - 20;
        offsetleft = 0;
    }else{
        offsetTop = elem.firstChild.clientHeight - 10;
        offsetleft = -90;
    }

    return {height: elem.childNodes[2].clientHeight, width: elem.childNodes[2].clientWidth, top: Math.round(top) + offsetTop , left: Math.round(left) + offsetleft };
}