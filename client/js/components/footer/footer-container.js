import {connect} from 'react-redux';
import {loadLanguage} from '../../actions/login-actions';
import {bindActionCreators} from 'redux';
import Footer from './footer';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
		loadLanguage: bindActionCreators(loadLanguage, dispatch)
    };
};

const footerContainer = connect(mapStateToProps, mapDispatchToProps)(Footer);
export default footerContainer;
