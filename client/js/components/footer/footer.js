import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import language from '../../../assets/text/languages.json';
import {forEach, findIndex, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import { getLocalizedElement } from '../../util.js';
import '../../../css/footer.css';

export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }

	changeLanguage( langue ){
		this.props.loadLanguage(language[langue]);
	}
	
    render() {
		return(
			<div id="footer">
                <div id="footerContainer">
                    <div id="footerLogo">
					</div>
					<div id="footerAdress">
					{getLocalizedElement(this.props.language["GameFlow-Adresse"])}
					</div>
					<div id="legalInfo">
						{getLocalizedElement(this.props.language["GameFlow-LegalInfo"])}
					</div>
					<div id="copyright">
						{getLocalizedElement(this.props.language["GameFlow-Copyright"])}
					</div>
                </div>
				
            </div>
		)
	}
}

Footer.propTypes = {
    language: PropTypes.object,
    lab: PropTypes.string,
    openNotification: PropTypes.func,
    hasNotification: PropTypes.bool
};
