import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import Carousel from '../mainTarget/Elements/carousel-container';
import {options} from '../../appConfig.js';
import $ from 'jquery';
import {forEach, findIndex, isEqual} from 'lodash';
import {Icon} from 'react-fa';

import '../../../css/header.css';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedItem: "",
        };
        this.headerElements = [{name: "header-reception", link: "/reception"},
            {name: "header-game", link: "/Jeux"},
			{name: "header-seriousgame", link: "/SeriousGames"},
            {name: "header-us", link: "/nous"},
            {name: "header-contact", link: "/contact"}];
    }

    changeLanguage(langue) {
        this.props.loadLanguage(langue);
    }

    handleClick(id) {
        this.setState({selectedItem: id});
    }

    render() {
        const context = this;
        const isMenuFixed = this.props.bannerClass === "bannerContainerFixed";

        const headerItems = this.headerElements.map(function (item, i) {
            return <Link to={item.link} key={i}
                         className={item.name === context.state.selectedItem ? "headerItem selected" : "headerItem"}
                         onClick={context.handleClick.bind(context, item.name)}>
              <span>{context.props.language[item.name]}
              </span>
            </Link>
        });

        const language = <div className="languageContainer">
            <div className="languageItemEN" onClick={this.changeLanguage.bind(this, "EN")}>
            </div>
            <div className="languageItemFR" onClick={this.changeLanguage.bind(this, "FR")}>
            </div>
        </div>;


        const carousel =  options.carouselVisible? <Carousel itemList={this.props.carouselItems}/> : "";

        const banner = <div id={"bannerContainer"}
            className={"noselect"}>
            <div id="headerItemContainer">
                {headerItems}
            </div>
            {language}
            <div id="followUsContainer">
                <a href={this.props.language["GameFlow-FaceBookLink"]}>
                    <div className="FaceBookMenu">
                    </div>
                </a>
				<a href={this.props.language["GameFlow-InstaLink"]}>
                    <div className="InstaMenu">
                    </div>
                </a>
            </div>
        </div>;

        return (
            <div id="header">
                <div id="headerContainer">
                    <div id="headerTitle">
                        <div className="headerImage">
                        </div>
                        <a href="/reception">
                            <div className="headerImageClickableZone">
                            </div>
                        </a>
						
						<div id="carouselWidget">
                            {carousel}
                        </div>
                            <div className="fixedExtraContainer"
                                 style={{opacity:isMenuFixed? 1 :0, height:isMenuFixed? "auto" :0}}>
                                <div className="blurEffect"/>
                                {banner}
                            </div>
                            <div>{banner}</div>
                    </div>

                </div>

            </div>
        );
    }
}


Header.propTypes = {
    language: PropTypes.object,
    lab: PropTypes.string,
    openNotification: PropTypes.func,
    hasNotification: PropTypes.bool
};
