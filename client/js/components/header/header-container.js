import {connect} from 'react-redux';
import {loadLanguage} from '../../actions/login-actions';
import {bindActionCreators} from 'redux';
import Header from './header';

const mapStateToProps = (state) => {
    return {
        language: state.language,
        bannerClass: state.main.bannerClass,
        carouselItems: state.main.carouselItems,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
		loadLanguage: bindActionCreators(loadLanguage, dispatch)
    };
};

const headerContainer = connect(mapStateToProps, mapDispatchToProps)(Header);
export default headerContainer;
