import React, {Component, PropTypes} from 'react';
import Header from '../header/header-container';
import Footer from '../footer/footer-container';
import FullscreenPopup from '../../fullscreenPopup/fullscreenPopup-container';

import {globalVariables} from '../../appConfig';
import {isEqual} from 'lodash';

import '../../../css/animations.css';
import '../../../css/main.css';
import '../../../css/shadows.css';

export default class Main extends Component {
    constructor(props) {
        super(props);
    }

    handleScroll(event){

        let scrollTop = event.target.scrollTop;
        if(scrollTop >= globalVariables.scrollTop && this.props.bannerClass === "bannerContainer"){
            this.props.updateBannerClass("bannerContainerFixed");
        }else if(scrollTop < globalVariables.scrollTop && this.props.bannerClass === "bannerContainerFixed"){
            this.props.updateBannerClass("bannerContainer");
        }
    }

    render() {
        return (
			<div className="pageContainer" onScroll={this.handleScroll.bind(this)}>
                <FullscreenPopup/>
                <div className="container">
                    <Header />
                    {this.props.submenu}
                    <div className="main">
                        {this.props.main ? this.props.main : ''}
                    </div>
                    <Footer />
                </div>
			</div>
        );
    }
}


/**
* @property {Object} [language] - This object contain all the text translations.
* @property {Object} [choosen] - This object contain the info about the site and paillas.
* @property {Object} [logInfo] - This is the user information.
* @property {Object} [submenu] - This is the sub menu to show.
* @property {Object} [menu] - This is the menu to display.
* @property {Object} [main] - This is the page "section" to display.
* @property {array} [analyseurs] - This is a list of all the analyseurs, we display it in "modules" display.
* @property {array} [analyse] - This is a list of all the analyses, we display it in "analyses" display.

* @property {function} [broadcastEvent] - This function is used to toggle the anaOpen state.
* @property {function} [saveAnalyseurs] - This function will overwright the new value of the analyseurs list.
*/
Main.propTypes = {
    language: PropTypes.object,
    choosen: PropTypes.object,
    logInfo: PropTypes.object,
    submenu: PropTypes.object,
    menu: PropTypes.object,
    main: PropTypes.object,
    analyseurs: PropTypes.array,
    analyses: PropTypes.array,
    loadLanguage: PropTypes.func,
    loadAnalyseurs: PropTypes.func,
    loginWithStorage: PropTypes.func,
    loadRoutesInfo: PropTypes.func,
    chooseFromStorage: PropTypes.func,
    loadAllAnalyses: PropTypes.func,
    loadConnectionsInfo: PropTypes.func,
};
