import {connect} from 'react-redux';
import {loadLanguage} from '../../actions/login-actions';
import {updateBannerClass} from '../../actions/main-actions';
import {bindActionCreators} from 'redux';
import Main from './main';

const mapStateToProps = (state) => {
    return {
        language: state.language,
        bannerClass: state.main.bannerClass,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadLanguage: bindActionCreators(loadLanguage, dispatch),
        updateBannerClass: bindActionCreators(updateBannerClass, dispatch),
    };
};

const mainContainer = connect(mapStateToProps, mapDispatchToProps)(Main);
export default mainContainer;
