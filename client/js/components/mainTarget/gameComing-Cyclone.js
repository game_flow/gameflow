import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/gameComing.css';
import { getLocalizedElement } from '../../util.js';
import GameDescription from './Elements/gameDescription.js';

export default class GameComingCyclone extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer gameComingContainer-cy">
                <div className="leftPart">
					<GameDescription language={this.props.language} name="cyclone"/>
                </div>
                <div className="rightPart">
					<div className="gameOutTttV textModule">
						<b>{this.props.language["cyclone-TrailerText"]}</b>
						<iframe width="295" height="250" src={this.props.language["cyclone-Trailer"]} frameBorder="0" allowFullScreen></iframe>
                    </div>
					<div className="gameOutDLRules textModule">
						<a href={this.props.language["cyclone-rules"]} download="Rules_DDD.pdf" >
							<div className="gameOutDLRulesLeftPart">

							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
								{this.props.language["chimere-downladRulesTitle"]}
								</p>
								<p className="pdfDLContent">
								{this.props.language["chimere-downladRules"]}
								</p>
							</div>
						</a>
                    </div>
					<div className="gameComingGameInfos textModule"> 
						<img  className="gameComingSplash" src={this.props.language["cyclone-gameInfos"]} />	
					</div>
					<div className="gameOutBuy textModule">
						<a href={this.props.language["chimere-WhereBuyLink"]} className="WhereBuyLink">
							 <div className="WhereBuy">
								<p className="WhereBuyTitle">
								{this.props.language["chimere-WhereBuyTitle"]}
								</p>
								<p className="WhereBuyInfos">
								{this.props.language["chimere-WhereBuy"]}
								</p>
							</div>
						</a>
					</div>
					<div className="gameOutPartner textModule">
						<h4>{this.props.language["cyclone-partners"]}</h4>
						{getLocalizedElement(this.props.language["cyclone-authors"])}
						<br/>
					</div>
							
					<div className="gameOutPartner textModule">	
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["cyclone-partnerIllusLink"]} className="partnerTextLink">
								{this.props.language["cyclone-partnerIllusName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["cyclone-partnerIllusLink"]}>
							<img 
								alt="test"
								src={this.props.language["cyclone-partnerIllusLogo"]}
								className="partnerLink" />
							</a>
						</div>		
					</div>
					<div className="gameOutPartner textModule">		
						<div className="gameOutPartnersTextPart">
							{this.props.language["5empereurs-partnerDistribInfo"]}
							<br/>
								<a href={this.props.language["5empereurs-partnerDistribLink"]} className="partnerTextLink">
								{this.props.language["5empereurs-partnerDistribName"]}
								</a>
						</div>
						<div className="gameOutPartnerLogo">
								<a href={this.props.language["5empereurs-partnerDistribLink"]}>
								<img 
									src={this.props.language["5empereurs-partnerDistribLogo"]}
									className="partnerLink" />
								</a>
						</div>     
					</div>

				</div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

GameComingCyclone.propTypes = {
    language: PropTypes.object.isRequired
};