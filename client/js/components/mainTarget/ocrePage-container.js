import {connect} from 'react-redux';
import OcrePage from './ocrePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(OcrePage);
export default bookPageContainer
