import React, {Component, PropTypes} from 'react';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/gameComing.css';
import { getLocalizedElement } from '../../util.js';
import GameDescription from './Elements/gameDescription.js';
import {Link} from 'react-router';

export default class GameComingAffinityExt extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer gameComingContainer-co">
                <div className="leftPart">
					          <GameDescription language={this.props.language} name="affinityExt"/>
          					{/* <div className="gameOutTttV textModule">
                                  <iframe width="550" height="250" src={this.props.language["complicity-tttv"]} frameBorder="0" allowFullScreen>
          						</iframe>
          					</div> */}

                      <div className="gameOutTttV textModule">
                          {/*    <Link to="/affinityExtension"> */}

                            {getLocalizedElement (this.props.language["affinityExt-descriptionFin"])}
                          {/*  </Link> */}
                      </div>

                        <div className="gameOutTttV textModule">
                          <Link to="/affinity">
                              <img  className="gameComingImgLink" src={this.props.language["affinityExt-extensionLink"]} />
                            </Link>
                        </div>


				    </div>
                <div className="rightPart">
					<div className="gameOutTttV textModule">
						<b>{this.props.language["complicity-LudoVox"]}</b>
						<iframe width="295" height="250" src={this.props.language["complicity-ludochrono"]} frameBorder="0" allowFullScreen></iframe>
                    </div>
                    <div className="gameOutDLRules textModule">
						<a href={this.props.language["affinityExt-rules"]} download="affinityExt_rules.pdf" >
							<div className="gameOutDLRulesLeftPart">

							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
								{this.props.language["chimere-downladRulesTitle"]}
								</p>
								<p className="pdfDLContent">
								{this.props.language["chimere-downladRules"]}
								</p>
							</div>
						</a>
                    </div>
					<div className="gameComingGameInfos textModule">
						<img  className="gameComingSplash" src={this.props.language["affinityExt-gameInfos"]} />
					</div>
                    <div className="gameOutBuy textModule">
						<a href={this.props.language["chimere-WhereBuyLink"]} className="WhereBuyLink">
							 <div className="WhereBuy">
								<p className="WhereBuyTitle">
								{this.props.language["chimere-WhereBuyTitle"]}
								</p>
								<p className="WhereBuyInfos">
								{this.props.language["chimere-WhereBuy"]}
								</p>
							</div>
						</a>
					</div>
					<div className="TshirtContainer">
						<a href={this.props.language["GameFlow-AffinityShirt"]} target="_blank" >
							<div className="AffinityshirtLink">
							</div>
						</a>
					</div>
					<div className="gameOutPartner textModule">
                        <h4>{this.props.language["complicity-partners"]}</h4>
						{getLocalizedElement(this.props.language["complicity-authors"])}
						<br/>
					</div>

                    <div className="gameOutPartner textModule">
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["complicity-partnerIllusLink"]} className="partnerTextLink">
								{this.props.language["complicity-partnerIllusName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["complicity-partnerIllusLink"]}>
							<img
								alt="test"
								src={this.props.language["complicity-partnerIllusLogo"]}
								className="partnerLink" />
							</a>
						</div>
                    </div>
					<div className="gameOutPartner textModule">
						<div className="gameOutPartnersTextPart">
							{this.props.language["5empereurs-partnerDistribInfo"]}
							<br/>
							<a href={this.props.language["5empereurs-partnerDistribLink"]} className="partnerTextLink">
								{this.props.language["5empereurs-partnerDistribName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["5empereurs-partnerDistribLink"]}>
							<img
								src={this.props.language["5empereurs-partnerDistribLogo"]}
								className="partnerLink" />
							</a>
						</div>
					</div>

                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

GameComingAffinityExt.propTypes = {
    language: PropTypes.object.isRequired
};
