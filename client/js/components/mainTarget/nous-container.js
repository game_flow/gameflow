import {connect} from 'react-redux';
import Nous from './nous';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const nousContainer = connect(mapStateToProps, mapDispatchToProps)(Nous);
export default nousContainer
