import {connect} from 'react-redux';
import puzzlephobosPage from './puzzlephobosPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(puzzlephobosPage);
export default bookPageContainer
