import React, {Component, PropTypes} from 'react';

import ReactDOM from 'react-dom';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/reception.css';
import { getLocalizedElement } from '../../util.js';
import NewsLetter from './Elements/newsLetter.js';
import {Link} from 'react-router';
import '../../../css/bookCollection.css';

export default class Reception extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        return (
            <div className="tabContainer receptionContainer">
                <div className="leftPart">
					<div className="receptionDescription textModule">
                        {getLocalizedElement(this.props.language["startPage-description"])}

						<div className="receptionListeJeux textModule">
							< div className = "orangeDivisor" >
								<h4>{getLocalizedElement(this.props.language["startPage-hotStuff"])}</h4>

							</div>

              <div className="GameMini">
                <div className="GameMiniImageContainer">
                  <Link to="/jungle">
                  <img
                  src={this.props.language["game-jungleMini"]}
                  alt={this.props.language["game-jungleName"]}
                  className="gameMiniLink" />
                  </Link>
                </div>
                <div className="GameMiniTextContainer">
                <Link to="/jungle" className="receptionGameTextLink">
                  {this.props.language["game-jungleName"]}
                </Link>
                <br/>
                <br/>
                <br/>
                {this.props.language["game-tocome02"]}
                </div>
              </div>
              <div className="Separate">
                <hr className="separation"/>
              </div>

              <div className="GameMini">
                <div className="GameMiniImageContainer">
                  <Link to="/puzzleocre">
                  <img
                  src={this.props.language["game-puzzleocreMini"]}
                  alt={this.props.language["game-puzzleocreName"]}
                  className="gameMiniLink" />
                  </Link>
                </div>
                <div className="GameMiniTextContainer">
                <Link to="/puzzleocre" className="receptionGameTextLink">
                  {this.props.language["game-puzzleocreName"]}
                </Link>
                <br/>
                <br/>
                <br/>
                {this.props.language["game-tocome01"]}
                </div>
              </div>
              <div className="Separate">
                <hr className="separation"/>
              </div>


              <div className="GameMini">
                <div className="GameMiniImageContainer">
                  <Link to="/puzzledragon">
                  <img
                  src={this.props.language["game-puzzledragonMini"]}
                  alt={this.props.language["game-puzzledragonName"]}
                  className="gameMiniLink" />
                  </Link>
                </div>
                <div className="GameMiniTextContainer">
                <Link to="/puzzledragon" className="receptionGameTextLink">
                  {this.props.language["game-puzzledragonName"]}
                </Link>
                <br/>
                <br/>
                <br/>
                {this.props.language["game-available"]}
                </div>
              </div>
              <div className="Separate">
                <hr className="separation"/>
              </div>

								<div className="GameMini">
								<div className="GameMiniImageContainer">
										<Link to="/premiereAventure">
										<img
											alt={this.props.language["game-firstAvName"]}
											src={this.props.language["game-firstAvMini"]}
											className="gameMiniLink" />
										</Link>
								</div>
								<div className="GameMiniTextContainer">
									<Link to="/premiereAventure" className="receptionGameTextLink">
										{this.props.language["game-firstAvName"]}
									</Link>
									<br/>
									<br/>
									<br/>
									{this.props.language["game-available"]}
								</div>
							</div>
						</div>
					</div>
				</div>
                <div className="rightPart">
									<br/>
									<br/>
									<br/>
					<div className="gameOutTttV textModule">

						<iframe width="295" height="250" src={this.props.language["MPZ-puzzleocre-trailer"]} frameBorder="0" allowFullScreen></iframe>
                    </div>
									<br/>
									<br/>
									<br/>
					<div className="BookCollectionGameInfos textModule">
						<a href={this.props.language["firstAv-coloringsheets"]} download="Coloriages_PremiereAventure.pdf" >
							<img  className="BookCollectionSplash" src={this.props.language["firstAv-colo"]} />
						</a>
					</div>
					<div className="BookCollectionDLColo textModule">
						<a href={this.props.language["firstAv-coloringsheets"]} download="Coloriages_PremiereAventure.pdf" >
							<div className="gameOutDLRulesLeftPart">
							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
									{this.props.language["firstAv-downladcoloringsheetsTitle"]}
								</p>
								<p className="pdfDLContent">
									{this.props.language["firstAv-downladcoloringsheets"]}
								</p>
							</div>
						</a>
					</div>
					{/*<div className="carouselContainer">
						<Carousel width="100%" showArrows autoPlay infiniteLoop dynamicHeight showThumbs={false} interval={5000} transitionTime={1000}>
								<div>
									<img src="../assets/image/SlideAccueil/1.png"/>
									<p className="legend">
									</p>
								</div>
								<div>
									<img src="../assets/image/SlideAccueil/2.png"/>
									<p className="legend">
									</p>
								</div>
								<div>
									<img src="../assets/image/SlideAccueil/3.png"/>
									<p className="legend">
									</p>
								</div>
								<div>
									<img src="../assets/image/SlideAccueil/4.png"/>
									<p className="legend">
									</p>
								</div>
								<div>
									<img src="../assets/image/SlideAccueil/5.png"/>
									<p className="legend">
									</p>
								</div>
						</Carousel>
					</div>*/}
					<div className="receptionFollowUs">
					{/*<div className="TshirtContainer">
							<a href={this.props.language["GameFlow-AffinityShirt"]} target="_blank" >
								<div className="TshirtLink">
								</div>
							</a>
					</div>*/}
					<br/>
						<div className="receptionFollowUsReseau">
							<div className="receptionFollowUsReseauLeft textModule">
								<h3>{getLocalizedElement(this.props.language["contact-FollowUs"])}	</h3>
							</div>
							<div className="receptionFollowUsReseauRight">
								<a href={this.props.language["GameFlow-FaceBookLink"]}>
									<div className="FaceBookLink">
									</div>
								</a>
								<a href={this.props.language["GameFlow-InstaLink"]} >
									<div className="InstaLink">
									</div>
								</a>
							</div>
						</div>

					</div>
                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property {array} [analyseurs] - This is a list of all the analyseurs with theres childs and analyses .
*/

Reception.propTypes = {
    language: PropTypes.object.isRequired,
    analyseurs: PropTypes.array,
};
