import {connect} from 'react-redux';
import SeriousGames from './seriousGames';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const nousContainer = connect(mapStateToProps, mapDispatchToProps)(SeriousGames);
export default nousContainer
