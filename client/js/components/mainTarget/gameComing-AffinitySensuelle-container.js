import {connect} from 'react-redux';
import GameComingAffinityExt from './gameComing-AffinitySensuelle';

const mapStateToProps = (state) => {
    return {
        language: state.language,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const gameComingContainerAffinityExt = connect(mapStateToProps, mapDispatchToProps)(GameComingAffinityExt);
export default gameComingContainerAffinityExt
