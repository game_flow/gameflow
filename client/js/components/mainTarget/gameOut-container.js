import {connect} from 'react-redux';
import GameOut from './gameOut';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const gameOutContainer = connect(mapStateToProps, mapDispatchToProps)(GameOut);
export default gameOutContainer
