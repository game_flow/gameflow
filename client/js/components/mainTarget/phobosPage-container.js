import {connect} from 'react-redux';
import PhobosPage from './phobosPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(PhobosPage);
export default bookPageContainer
