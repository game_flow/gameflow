import {connect} from 'react-redux';
import CarouselItem from './carousel-item';
import {bindActionCreators} from 'redux';
import {updatePoster} from '../../../actions/main-actions';

const mapStateToProps = (state) => {
    return {
        language: state.language,
        refreshItems: state.main.refreshItems
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updatePoster: bindActionCreators(updatePoster, dispatch)
    }
};

const carouselItemContainer = connect(mapStateToProps, mapDispatchToProps)(CarouselItem);
export default carouselItemContainer
