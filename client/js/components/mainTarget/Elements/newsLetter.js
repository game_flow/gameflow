import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {Icon} from 'react-fa';
import '../../../../css/newsLetter.css';

export default class NewsLetter extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
			<div className="NewsLetter">
				<div className="newsLeftPart">
					<img className="newsImage" src="../assets/image/IconeNewsLetter.png" />
				</div>
				<div className="newsRightPart">
					<div className="LastNewsLetter textModule">
						<a href={this.props.language["startPage-NewsLink"]} className="newsLink">
							 <div className="newsLinkRightPart">
								{this.props.language["startPage-SeeLastNews"]}
							</div>
						</a>
					</div>
					<div className="InscriptionNewsLetter textModule">
						<a href={this.props.language["startPage-NewsInscriptionLink"]} className="newsLink">
							<div className="newsInscriptionRightPart">
								{this.props.language["startPage-SubscribeNews"]}
							</div>
						</a>
					</div>
				</div>
			</div>
		);
	}
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

NewsLetter.propTypes = {
	language: PropTypes.object.isRequired
};