import {connect} from 'react-redux';
import Carousel from './carousel';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const carouselContainer = connect(mapStateToProps, mapDispatchToProps)(Carousel);
export default carouselContainer
