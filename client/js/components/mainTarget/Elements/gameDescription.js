import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../../css/gameOut.css';
import { getLocalizedElement } from '../../../util.js';

export default class GameDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {};
		this.name = this.props.name ;    
    }


    render() {
		const context = this;
		var logo = this.props.name.concat("-logo");
		var date = this.props.name.concat("-date");
		var catchphrase = this.props.name.concat("-catchphrase");
		var desc1 = this.props.name.concat("-description1");
		var splash = this.props.name.concat("-splash");
		var image1 = this.props.name.concat("-image");
		var imageDown = this.props.name.concat("-imageDown");
		var desc2 = this.props.name.concat("-description2");
        return (
			<div>
				<div className="gameOutReleaseDate"> 
					<img className="gameOutLogo" src={this.props.language[logo]}>
					</img>
					<br />
					{this.props.language[date]}
				</div>
				<div className="gameOutDescription textModule">
					<h3> {getLocalizedElement(this.props.language[catchphrase])} </h3>
					<div className="gameOutDescriptionPart textModule">
						<div className="gameOutHalfDescription textModule">
							{getLocalizedElement(this.props.language[desc1])}
						</div>
						<div className="gameOutHalfDescription textModule">
							<img  className="gameOutSplash" src={this.props.language[splash]} />
						</div>
					</div>
					<div className="gameOutDescriptionPart textModule" >
						<div className="gameOutHalfDescription textModule">
							<img  className="gameOutSplash" src={this.props.language[image1]} />
						</div>
						<div className="gameOutHalfDescription textModule">
							{getLocalizedElement(this.props.language[desc2])}
						</div>
					</div>
					<div className="Book3D textModule">
						<img  className="gameOutSplash" src={this.props.language[imageDown]} />
					</div>
				</div>
			</div>	
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property [name] - name of the game.
*/

GameDescription.propTypes = {
    language: PropTypes.object.isRequired,
	name : PropTypes.string,
	
	
};