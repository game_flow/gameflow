import {connect} from 'react-redux';
import CarouselDateItem from './carousel-dateItem';
import {bindActionCreators} from 'redux';
import {updatePoster} from '../../../actions/main-actions';

const mapStateToProps = (state) => {
    return {
        language: state.language,
        refreshItems: state.main.refreshItems
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const carouselDateItemContainer = connect(mapStateToProps, mapDispatchToProps)(CarouselDateItem);
export default carouselDateItemContainer
