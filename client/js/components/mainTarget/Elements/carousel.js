import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../../css/carousel.css';
import {globalVariables} from '../../../appConfig';
import CarouselItem from './carousel-item-container';
import CarouselDateItem from './carousel-dateItem-container';



export default class Carousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrollerWidth:0,
            scrollPosition:0,
            carouselWidth: 0
        };
        this.startPoint = 0;
        this.sortedList = [];
        this.dragStartPosition = 0;
        this.pressTime;
        this.isClickDown = false;
    }

    componentDidMount(){
        const context = this;
        const carousel = document.getElementsByClassName('carousel-items')[0];
        const scroller = document.getElementsByClassName('carousel-scroller')[0];

        this.carouselWidth = carousel.clientWidth;

        this.sortedList = sortBy(this.props.itemList,'timestamp');
        const stringDate = new Date().toString().slice(11,15) + '-01-01T10:00:01Z';
        this.startPoint = new Date(stringDate).getTime() ;

        this.firstYear =  parseInt(new Date(this.sortedList[0].timestamp).toString().slice(11,15));
        this.carouselStart = new Date(this.firstYear + '-' + '01-01T10:00:01Z').getTime();
        let lastYear =  parseInt(new Date(this.sortedList[this.sortedList.length -1].timestamp + globalVariables.secondeInYear).toString().slice(11,15));
        const curentYear = new Date().toString().slice(11,15);
        if(lastYear < curentYear){
            lastYear = curentYear;
        }

        this.numberYearsToDisplay = lastYear - this.firstYear +1;

        this.setState({
            scrollerWidth: (this.numberYearsToDisplay) * globalVariables.carousel.yearSize,
            scrollPosition:this.fromTimeToDistance( new Date().getTime()) - (document.getElementsByClassName('carousel-items')[0].clientWidth)/3 // start with turtle in middlle of the carouselle
        }); // +1 in order to be able tu put the current date in the middle we need extra background
    }

    componentDidUpdate(){
        const carousel = document.getElementsByClassName('carousel-items')[0];
        this.carouselWidth = carousel.clientWidth;
    }

    fromTimeToDistance(time){
        const distance =  parseInt(((time - this.carouselStart) / globalVariables.secondeInYear) * globalVariables.carousel.yearSize);
        return parseInt(distance);
    }

    fromDistanceToTime(distance){
        const duration = (distance / globalVariables.carousel.yearSize) * globalVariables.secondeInYear;
        return parseInt(duration);
    }

    handleScroll(type,e){
        moveBackground(type, this.state.scrollPosition, globalVariables.carousel.clickStep, this);
        /*let newScrollPosition = this.state.scrollPosition;
        if(type === "left" && (newScrollPosition -  globalVariables.carousel.clickStep)> 0 ){
            newScrollPosition = newScrollPosition -  globalVariables.carousel.clickStep; // need to stick to second frise for no scroll jump
            this.setState({scrollPosition : newScrollPosition});
        }else if(type === "right" && newScrollPosition < (this.numberYearsToDisplay * globalVariables.carousel.yearSize - this.carouselWidth) - globalVariables.carousel.clickStep ){ //right
            newScrollPosition = newScrollPosition + globalVariables.carousel.clickStep;
            this.setState({scrollPosition : newScrollPosition});
        }*/
    }
    handleMouseDown(type,e) {
           setTimeout(function () {
               moveBackground(type, context.state.scrollPosition, 5, context);
           }, 100,this.handleMouseDown());
    }

    handleMouseUp(){
        this.isClickDown= false;
        clearTimeout(this.pressTime);
        //moveBackground(type, this.state.scrollPosition, this);
    }

    handleTurtleOver(){}

    onDragEnter(e){
    /*    this.dragStartPosition = e.pageX;
        this.isClickDown = true;*/
    }

    onDrag(e){
    /*    const newScrollPosition =  this.state.scrollPosition + this.dragStartPosition - e.pageX;
        if(this.isClickDown && newScrollPosition - globalVariables.carousel.clickStep > 0 &&
            newScrollPosition < (this.numberYearsToDisplay * globalVariables.carousel.yearSize - this.carouselWidth) ){
            this.dragStartPosition =  e.pageX;
            this.setState({scrollPosition : newScrollPosition});
        }*/
    }

    onDragStop(e){
       /* this.isClickDown = false;*/
    }


    render() {
        const context = this;
        let carouselItems = [];
        let  carouselDateItems = [];
        const curentDate = new Date().getTime(); // This is the current date in timestamp

        for(let i=0; i <= this.numberYearsToDisplay; i++){
            const dateString = (context.firstYear + i) + '-' + '01-01T10:00:01Z';
            const item = {
                name:(context.firstYear + i),
                timestamp: new Date(dateString).getTime(),
                size:2,
                text:(context.firstYear + i),
                type:"year",
                highlight:false
            };
            carouselDateItems.push(<CarouselDateItem key={"carouselDateItem_" + i}
                                                     id={"carouselDateItem_" + i}
                                                     item={item}
                                                     mirror={0}
                                                     marginLeft={context.fromTimeToDistance(item.timestamp) - context.state.scrollPosition - globalVariables.yearTextSeize/2}
            />);
        }

        this.sortedList.map(function (item,i) {
            if( context.fromTimeToDistance(item.timestamp) > (context.state.scrollPosition - 200 )&&
                context.fromTimeToDistance(item.timestamp) < context.state.scrollPosition + context.fromDistanceToTime(context.carouselWidth) + 100){
                carouselItems.push(<CarouselItem key={"carouselItem_" + i}
                                                 id={"carouselItem_" + i}
                                                 item={item}
                                                 type={item.type}
                                                 subType={i % 2 === 0? "top":"bottom"}
                                                 imagePath={context.props.imageFolder}
                                                 marginLeft={context.fromTimeToDistance(item.timestamp) - context.state.scrollPosition}
                                                 scrollPosition={context.state.scrollPosition}
                                                 carouselWidth={context.carouselWidth}
                />);
            }
        });
        carouselItems.reverse();

        return (
            <div className={"carousel-widget-container" + " " + this.props.language["thisFilLanguage"]}
                 onMouseEnter={this.onDragStop.bind(this)}>
                <div className="carousel-borderStart"/>
                <div className="carousel-widget">
                    <div className="carousel-borderStart"/>
                    <div className="carousel-leftArrow"
                         onMouseDown={this.handleMouseDown.bind(this,'left')}
                         onMouseUp={this.handleMouseUp.bind(this,'left')}
                         onClick={this.handleScroll.bind(this,'left')}/>
                    <div className="carousel-rightArrow"
                         onClick={this.handleScroll.bind(this,'right')}
                         onMouseDown={this.handleMouseDown.bind(this,'right')}
                         onMouseUp={this.handleMouseUp.bind(this,'right')}/>
                    {carouselItems}
                    {carouselDateItems}
                    <div className="carousel-items">
                        <div className="carousel-scroller"
                             onMouseDown={this.onDragEnter.bind(this)}
                             onMouseMove={this.onDrag.bind(this)}
                             onMouseUp={this.onDragStop.bind(this)}
                             style={{width: this.state.scrollerWidth + 'px', marginLeft: - this.state.scrollPosition - globalVariables.carousel.seasonOffset}}>
                            <div className="carousel-trail" style={{width:this.fromTimeToDistance(curentDate)}}/>
                            <div className="carousel-nowTurtule" onMouseOver={this.handleTurtleOver.bind(this)}/>
                        </div>
                    </div>
                </div>
                <div className="carousel-borderEnd"/>
            </div>
        );
    }
}
/**
 * propTypes
 * @property {Object} [language] - This object contain all the text translations.
 * @property {Object} [name] - name of the game.
 * @property (array} [item] - This is an object containing all the carouselItem info.
 * @property (string} [imageFolder] - This is the path to the image directory.
 */

Carousel.propTypes = {
    language: PropTypes.object.isRequired,
    itemList: PropTypes.array,
    imageFolder: PropTypes.string,

};

function moveBackground(type, newScrollPosition, movment, context){
    if(type === "left" && (newScrollPosition -  movment)> 0 ){
        newScrollPosition = newScrollPosition -  movment; // need to stick to second frise for no scroll jump
        context.setState({scrollPosition : newScrollPosition});
    }else if(type === "right" && newScrollPosition < (context.numberYearsToDisplay * globalVariables.carousel.yearSize - context.carouselWidth) - movment ){ //right
        newScrollPosition = newScrollPosition + movment;
        context.setState({scrollPosition : newScrollPosition});
    }
}