import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {Icon} from 'react-fa';

export default class Partner extends Component {

    constructor(props) {
		super(props);
        this.state = {};
		this.name = props.name;
		this.logo = props.logo;
		this.link = props.link;
		this.info = props.info;        
    }


    render() {
        return (
				<div>
				{this.name};
				</div>
			)
	}
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property [name] - name of the partner.
*/

Partner.propTypes = {
	name : PropTypes.string,
	info : PropTypes.string,
	logo : PropTypes.string,
	link : PropTypes.string,
};