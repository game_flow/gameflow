import React, {Component, PropTypes} from 'react';
import {globalVariables, options} from '../../../appConfig';
import {getCoords} from '../../../util';
import {getLocalizedElement } from '../../../util.js';
import {getGlobalOffset} from '../../../util';

export default class CarouselItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFocused:false,
           // isFullScreen:false,
        };
        this.safetyZone = 40;
        this.carouselWidth =  this.props.carouselWidth - 50;
        this.canHoverAgain = true;
        //this.carouselBigPosterDimentions = 140; //to match with the css value
    }



    onClick(e){
        if(e.target.className === "carousel-image" && this.props.marginLeft > this.safetyZone && this.props.marginLeft <  this.carouselWidth - this.safetyZone){
            const imagePath = options.bigImagePath + this.props.item.imageSource;
            const posterElement = document.getElementById(this.props.id);
            this.props.updatePoster(imagePath, true, this.props.item) //, {top:locationObject.top, left:locationObject.left , width: locationObject.width, height: locationObject.height}
        }
    }

    mouseEnter(e){
        if(this.props.marginLeft > this.safetyZone && this.props.marginLeft <  this.carouselWidth - this.safetyZone && this.canHoverAgain){
            this.setState({isFocused:true});
        }
    }

    mouseLeave(e){
        if(this.props.marginLeft > this.safetyZone && this.props.marginLeft <  this.carouselWidth - this.safetyZone){
            this.setState({isFocused:false});
        }
    }

    fingerClick(){
        const context = this;
        if( this.props.marginLeft > this.safetyZone &&
            this.props.marginLeft <  this.carouselWidth - this.safetyZone){
            if(this.state.isFocused){
                context.canHoverAgain = false;
                setTimeout(function(){ context.canHoverAgain = true;}, 1000);
                this.setState({isFocused:false});
            }else{
                this.setState({isFocused:true});
            }
        }
    }

    fingerClickBig(e){
        if(this.props.item.hasOwnProperty('bigHoverZone') && this.props.item.bigHoverZone){
            this.fingerClick();
            e.stopPropagation();
            e.preventDefault();
        }
    }


    loadLink(){
        window.open(this.props.item.link)
    }

   render() {
       const  myStyle = {marginLeft:this.props.marginLeft};
       const type = this.state.isFocused ? this.props.subType + " focused" : this.props.subType;
       const iconClass = this.props.item.hasOwnProperty('bigHoverZone') && this.props.item.bigHoverZone? " big" : "";

       const imagePath =  options.accesPath + this.props.item.imageSource;
       const endBoundary = document.getElementsByClassName('carousel-items')[0].clientWidth - globalVariables.carousel.faidingDitance;

       let parchoContainerClasss = "carousel-item-container " + type; "leftParcho";
       if( this.props.marginLeft  + globalVariables.parcheSeize> endBoundary ){
           parchoContainerClasss = parchoContainerClasss + " leftParcho";
       }
        return (
			<div className={parchoContainerClasss}
                 id={this.props.id}
                 onMouseLeave={this.mouseLeave.bind(this)}
                 onClick={this.onClick.bind(this)}
                 style={myStyle}>
                <div className="carousel-marqueur"/>
                <div className="carousel-inconClikZone"
                     onClick={this.fingerClick.bind(this)}
                     onMouseEnter={this.mouseEnter.bind(this)}/>
                <div className="carousel-item-forground"/>
                <div className={"carousel-icone " + this.props.item.type  + iconClass}
                     onClick={this.fingerClickBig.bind(this)}/>
                 <div className="carousel-image"
                      style={{backgroundImage: "url(" +imagePath+ ")"}}/>
                <div className="parcho">
                    {this.props.item.hasOwnProperty('link')?
                    <div className={"carousel-icone-link"} title={this.props.language["linkText"]} onClick={ this.loadLink.bind(this)} /> : "" }
                    <div className="parchoDate">{this.props.item.date} </div>
                    <div className="parchoText">{getLocalizedElement(this.props.item.text)} </div>
                </div>
			</div>
        );
   }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property {Object} [name] - name of the game.
* @property {Object} [item] - This is an object containing all the carouselItem info.
 * @property {Int} [mirror] - true if it is a top of the bottom.
 * @property {String} [imagePath] - This is the path to the image directory..
*/

CarouselItem.propTypes = {
    language: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    mirror: PropTypes.number,
    imagePath: PropTypes.string,
	
};
				