import React, {Component, PropTypes} from 'react';
import {globalVariables} from '../../../appConfig';
import {getCoords} from '../../../util';

import {getGlobalOffset} from '../../../util';

export default class CarouseDatelItem extends Component {

   render() {
        let type = this.props.mirror? "top":"bottom";
        const  myStyle = {marginLeft:this.props.marginLeft,opacity:1};
        return (
			<div className={ "carousel-dateItem-container " + type}
                 id={this.props.id}
                 style={myStyle}>
                <div className="noselect carousel-dateItem-container">{this.props.item.text}</div>
			</div>
        );
   }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property {Object} [name] - name of the game.
* @property {Object} [item] - This is an object containing all the carouselItem info.
 * @property {Int} [mirror] - true if it is a top of the bottom.
 * @property {String} [imagePath] - This is the path to the image directory..
*/

CarouseDatelItem.propTypes = {
    language: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    mirror: PropTypes.number,
    imagePath: PropTypes.string,
	
};
				