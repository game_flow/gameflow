import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../../css/gameOut.css';
import '../../../../css/bookCollection.css';
import { getLocalizedElement } from '../../../util.js';

export default class BookCollection extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }
	
   render() {
		const context = this;
        return (
			<div>
				<div className="gameOutReleaseDate"> 
					<img className="BookCollectionSplash" src={this.props.language["firstAv-logo"]}>
					</img>
					<br />
				</div>
				<div className="BookCollectionSplash textModule">
							{getLocalizedElement(this.props.language["firstAv-description"])}
				</div>
				
				<div className="gameOutBuy textModule">
					<a href={this.props.language["firstAv-DiscoverLink"]} className="WhereBuyLink">
						 <div className="WhereBuy">
							<p className="WhereBuyTitle">
							{this.props.language["firstAv-DiscoverTitle"]}
							</p>
							<p className="WhereBuyInfos">
							{this.props.language["firstAv-Discover"]}
							</p>
						</div>
					</a>
				</div>
				
				<div className="BookTttV textModule"> 
					<b>{this.props.language["firstAv-LudoVox"]}</b>
					<iframe width="295" height="250" src={this.props.language["firstAv-ludochrono"]} frameBorder="0" allowFullScreen></iframe>
				</div>	
				<div className="gameOutBuy textModule">
					<a href={this.props.language["firstAv-WhereBuyLink"]} className="WhereBuyLink">
						 <div className="WhereBuy">
							<p className="WhereBuyTitle">
							{this.props.language["firstAv-WhereBuyTitle"]}
							</p>
							<p className="WhereBuyInfos">
							{this.props.language["firstAv-WhereBuy"]}
							</p>
						</div>
					</a>
				</div>
				{getLocalizedElement(this.props.language["firstAv-WhereBuyExpl"])}
								
			</div>
   );
   }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property [name] - name of the game.
*/

BookCollection.propTypes = {
    language: PropTypes.object.isRequired,
	
};
				