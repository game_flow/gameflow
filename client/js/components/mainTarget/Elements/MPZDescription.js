import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../../css/gameOut.css';
import { getLocalizedElement } from '../../../util.js';

export default class BookDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {};
		this.name = this.props.name ;
    }


    render() {
		const context = this;
		var logo = this.props.name.concat("-logo");
		var date = this.props.name.concat("-date");
		var catchphrase = this.props.name.concat("-catchphrase");
		var desc1 = this.props.name.concat("-description1");
		var splash = this.props.name.concat("-splash");
		var image1 = this.props.name.concat("-image");
		var desc2 = this.props.name.concat("-description2");
		var bookLink = this.props.name.concat("-bookLink");
		var book3D = this.props.name.concat("-book3d");

        return (
			<div>
				<div className="gameOutReleaseDate">
					<img className="gameOutLogo" src={this.props.language[logo]}>
					</img>
					<br />
          <br />
					{this.props.language[date]}
				</div>
				<br />
				<div className="gameOutTttV textModule">
					<iframe width="295" height="250" src={this.props.language["MPZ-puzzledragon-videoRegle"]} frameBorder="0" allowFullScreen></iframe>
                </div>
		{/*		<div className="gameOutBuy textModule">
						<a href={this.props.language[bookLink]} className="WhereBuyLink">
							 <div className="WhereBuy">
								<p className="WhereBuyTitle">
								{this.props.language["MPZ-bookDiscoverTitle"]}
								</p>
								<p className="WhereBuyInfos">
								{this.props.language["MPZ-Discover"]}
								</p>
							</div>
						</a>
					</div>
					<br /> */}
					<br />
				<div className="gameOutDescription textModule">
				{/*<h3> getLocalizedElement(this.props.language[catchphrase]) </h3>*/}
					<div className="gameOutDescriptionPart textModule">
						<div className="gameOutHalfDescription textModule">
							{getLocalizedElement(this.props.language[desc1])}
						</div>
						<div className="gameOutHalfDescription textModule">
							<img  className="gameOutSplash" src={this.props.language[splash]} />
						</div>
					</div>
					<div className="gameOutDescriptionPart textModule" >
						<div className="gameOutHalfDescription textModule">
							<img  className="gameOutSplash" src={this.props.language[image1]} />
						</div>
						<div className="gameOutHalfDescription textModule">
							{getLocalizedElement(this.props.language[desc2])}
						</div>
					</div>
				</div>
				{/*<div className="Book3D textModule">
					<a href={this.props.language[bookLink]}>
						<img  className="BookSplash" src={this.props.language[book3D]} />
					</a>
				</div>*/}

			</div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property [name] - name of the game.
*/

BookDescription.propTypes = {
    language: PropTypes.object.isRequired,
	name : PropTypes.string,


};
