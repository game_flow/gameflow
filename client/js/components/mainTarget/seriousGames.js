import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/seriousGames.css';
import { getLocalizedElement } from '../../util.js';

export default class SeriousGames extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer">
                <div className="leftPart seriousContainer">
                    <div className="seriousTitle textModule">
                        <h3> {this.props.language["serious-Title"]}</h3>
						{getLocalizedElement(this.props.language["serious-proposition"])}
						<br/>	<br/>
						{getLocalizedElement(this.props.language["serious-Description"])}
						<br/><br/>
						<h3> {this.props.language["serious-exempleTitle"]}</h3>

						<div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionText textModule">
								<h4> {this.props.language["serious-colasTitle"]}</h4>

							</div>
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-colasLogo"]} />
							</div>
						</div>
						{getLocalizedElement(this.props.language["serious-colasDescription"])}
						<br/><br/>
						<div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-SafeTeamlBox"]} />
							</div>
							<div className="seriousHalfDescriptionText textModule">
								{getLocalizedElement(this.props.language["serious-safeTeamDescription"])}
							</div>
						</div>


						{/*
						<div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionText textModule">
								<h4> {this.props.language["serious-sym2BTitle"]}</h4>

							</div>
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-sym2BLogo"]} />
							</div>
						</div>
						{getLocalizedElement(this.props.language["serious-sym2BDescription"])}
						<br/><br/>
						<div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-sym2BBox"]} />
							</div>
							<div className="seriousHalfDescriptionText textModule">
								{getLocalizedElement(this.props.language["serious-sym2BDescription2"])}
							</div>
						</div>
						*/}

            <div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionText textModule">
								<h4> {this.props.language["serious-EmoietMoiTitle"]}</h4>

							</div>
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-EmoietMoiLogo"]} />
							</div>
						</div>
						{getLocalizedElement(this.props.language["serious-CodaseDescription"])}
						<br/><br/>
						<div className="seriousDescriptionPart textModule" >
							<div className="seriousHalfDescriptionImg textModule">
								<img  className="seriousSplash" src={this.props.language["serious-EmoietMoilBox"]} />
							</div>
							<div className="seriousHalfDescriptionText textModule">
								{getLocalizedElement(this.props.language["serious-EmoietMoiDescription"])}
							</div>
						</div>

            <div className="seriousDescriptionPart textModule" >
              <div className="seriousHalfDescriptionText textModule">
                <h4> {this.props.language["serious-TrajectoiresTitle"]}</h4>

              </div>
              <div className="seriousHalfDescriptionImg textModule">
                <img  className="seriousSplash" src={this.props.language["serious-TrajectoiresLogo"]} />
              </div>
            </div>
            {getLocalizedElement(this.props.language["serious-SpacDescription"])}
            <br/><br/>
            <div className="seriousDescriptionPart textModule" >
              <div className="seriousHalfDescriptionImg textModule">
                <img  className="seriousSplash" src={this.props.language["serious-TrajectoireslBox"]} />
              </div>
              <div className="seriousHalfDescriptionText textModule">
                {getLocalizedElement(this.props.language["serious-TrajectoiresDescription"])}
              </div>
            </div>

                    </div>
				</div>
                <div className="rightPart">
					<br/><br/>
					{getLocalizedElement(this.props.language["serious-contactUs"])}
					<br/><br/>
					<h3> {this.props.language["serious-butTitle"]}</h3>
					{getLocalizedElement(this.props.language["serious-but"])}
					<br/><br/>
					<h3> {this.props.language["serious-whoTitle"]}</h3>
					{getLocalizedElement(this.props.language["serious-who"])}

                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

SeriousGames.propTypes = {
    language: PropTypes.object.isRequired
};
