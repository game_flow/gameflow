import React, {Component, PropTypes} from 'react';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/bookPage.css';
import { getLocalizedElement } from '../../util.js';
import BookDescription from './Elements/bookDescription.js';
import BookCollection from './Elements/BookCollection.js';


export default class junglePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer jungleContainer">
                <div className="leftPart">
					<BookDescription language={this.props.language} name="firstAv-jungle"/>
                </div>
                <div className="rightPart">

                <div>
      						<BookCollection  language={this.props.language} />
      					</div>
      					<div>
      						<hr className="separation"/>
      					</div>
      					<div className="BookPartner textModule">
                              <h4>{this.props.language["firstAv-partners"]}</h4>
      						{getLocalizedElement(this.props.language["firstAv-jungle-authors"])}
      					</div>


                          <div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["chimere-partnerIllusInfo"]}
      							<br/>
      							<a href={this.props.language["firstAv-jungle-partnerIllusLink"]} className="BookpartnerTextLink">
      								{this.props.language["firstAv-jungle-partnerIllusName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["firstAv-jungle-partnerIllusLink"]}>
      							<img
      								alt="test"
      								src={this.props.language["firstAv-jungle-partnerIllusLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
                          </div>
      					<div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["chimere-partnerDistribInfo"]}
      							<br/>
      							<a href={this.props.language["chimere-partnerDistribLink"]} className="BookpartnerTextLink">
      								{this.props.language["chimere-partnerDistribName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["chimere-partnerDistribLink"]}>
      							<img
      								src={this.props.language["chimere-partnerDistribLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
      					</div>
      					<div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["firstAv-partnerFabInfo"]}
      							<br/>
      							<a href={this.props.language["firstAv-ImagoPartnerFabLink"]} className="BookpartnerTextLink">
      								{this.props.language["firstAv-ImagoPartnerFabName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["firstAv-ImagoPartnerFabLink"]}>
      							<img
      								src={this.props.language["firstAv-ImagoPartnerFabLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
      					</div>


      				</div>
                  </div>
              );
          }
      }

      /**
      * propTypes
      * @property {Object} [language] - This object contain all the text translations.
      */

      junglePage.propTypes = {
          language: PropTypes.object.isRequired
      };
