import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/contact.css';
import NewsLetter from './Elements/newsLetter.js';
import { getLocalizedElement } from '../../util.js';

export default class Contact extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer contactContainer">
                <div className="leftPart">
                    <div className="contactLegal textModule">
                        <h1>Game Flow</h1>
						<div>
						{getLocalizedElement(this.props.language["contact-intro"])}
						</div>
						<br/>
						<div>
						{getLocalizedElement(this.props.language["contact-communication"])}
						</div>
						<br/>
						<div>
						{getLocalizedElement(this.props.language["contact-proto"])}
						</div>
						<br/>
						<div>
						{getLocalizedElement(this.props.language["contact-commande"])}
						</div>
						<br/>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Clem"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-ClemInfo"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Rom"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-RomInfo"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Gaet"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-GaetInfo"])}
							</div>
						</div>
            <hr className="separation"/>
            <div className="contactNous">
              <div className="contactNousLeft">
                {getLocalizedElement(this.props.language["contact-Xav"])}
              </div>
              <div className="contactNousRight">
                {getLocalizedElement(this.props.language["contact-XavInfo"])}	
              </div>
            </div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Margot"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-MargotInfo"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Caro"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-CaroInfo"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-Adress"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-Adresse"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-LegalInfo"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-LegalInfoPage"])}
							</div>
						</div>
						<hr className="separation"/>
						<div className="contactNous">
							<div className="contactNousLeft">
								{getLocalizedElement(this.props.language["contact-SiegeAdress"])}
							</div>
							<div className="contactNousRight">
								{getLocalizedElement(this.props.language["contact-Adresse"])}
							</div>
						</div>
					</div>
                </div>
                <div className="rightPart">
					<div className="contactReseau">
						<div className="contactReseauMidd textmodule">
							<img  className="contactGFLogoSplash" src={this.props.language["GameFlow-Logo"]} />
						</div>
						<div className="contactReseauLeft textModule">
							<h3>{getLocalizedElement(this.props.language["contact-FollowUs"])}	</h3>
						</div>
						<div className="contactReseauRight">
							<a href={this.props.language["GameFlow-FaceBookLink"]}>
								<div className="FaceBookLink">
								</div>
							</a>
							<a href={this.props.language["GameFlow-InstaLink"]} >
								<div className="InstaLink">
								</div>
							</a>
							</div>
					</div>

                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

Contact.propTypes = {
    language: PropTypes.object.isRequired
};
