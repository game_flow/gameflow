import {connect} from 'react-redux';
import AltlantidePage from './atlantidePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(AltlantidePage);
export default bookPageContainer
