import React, {Component, PropTypes} from 'react';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/bookPage.css';
import { getLocalizedElement } from '../../util.js';
import BookDescription from './Elements/bookDescription.js';
import BookCollection from './Elements/BookCollection.js';


export default class bibliothequePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer bibliothequeContainer">
                <div className="leftPart">				
					<BookDescription language={this.props.language} name="firstAv-bibliotheque"/>
                </div>
                <div className="rightPart">

                <div>
      						<BookCollection  language={this.props.language} />
      					</div>
      					<div>
      						<hr className="separation"/>
      					</div>
      					<div className="BookPartner textModule">
                              <h4>{this.props.language["firstAv-partners"]}</h4>
      						{getLocalizedElement(this.props.language["firstAv-bibliotheque-authors"])}
      					</div>
						<div className="BookPartner textModule">
							{this.props.language["firstAv-bibliotheque-orthoIntro"]}
      					</div>
						<div className="BookCollectionDLColo textModule">
							<a href={this.props.language["firstAv-bibliotheque-orthoFile"]} download="BibliothequeInfinie_LivretOrthophonie.pdf" >
								<div className="gameOutDLRulesLeftPart">
								</div>
								<div className="gameOutDLRulesRightPart">
									<p className="pdfDLTitle">
										{this.props.language["firstAv-bibliotheque-orthoDLTitle"]}
									</p>
									<p className="pdfDLContent">
										{this.props.language["firstAv-bibliotheque-orthoDLContent"]}
									</p>
								</div>
							</a>
							<br/>
							<br/>
						</div>

                          <div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["chimere-partnerIllusInfo"]}
      							<br/>
      							<a href={this.props.language["firstAv-bibliotheque-partnerIllusLink"]} className="BookpartnerTextLink">
      								{this.props.language["firstAv-bibliotheque-partnerIllusName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["firstAv-bibliotheque-partnerIllusLink"]}>
      							<img
      								alt="test"
      								src={this.props.language["firstAv-bibliotheque-partnerIllusLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
                          </div>
      					<div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["chimere-partnerDistribInfo"]}
      							<br/>
      							<a href={this.props.language["chimere-partnerDistribLink"]} className="BookpartnerTextLink">
      								{this.props.language["chimere-partnerDistribName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["chimere-partnerDistribLink"]}>
      							<img
      								src={this.props.language["chimere-partnerDistribLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
      					</div>
      					<div className="BookPartner textModule">
      						<div className="BookPartnersTextPart">
      							{this.props.language["firstAv-partnerFabInfo"]}
      							<br/>
      							<a href={this.props.language["firstAv-partnerFabLink"]} className="BookpartnerTextLink">
      								{this.props.language["firstAv-partnerFabName"]}
      							</a>
      						</div>
      						<div className="BookPartnerLogo">
      							<a href={this.props.language["firstAv-partnerFabLink"]}>
      							<img
      								src={this.props.language["firstAv-partnerFabLogo"]}
      								className="BookpartnerLink" />
      							</a>
      						</div>
      					</div>


      				</div>
                  </div>
              );
          }
      }

      /**
      * propTypes
      * @property {Object} [language] - This object contain all the text translations.
      */

      bibliothequePage.propTypes = {
          language: PropTypes.object.isRequired
      };
