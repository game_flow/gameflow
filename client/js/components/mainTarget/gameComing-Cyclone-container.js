import {connect} from 'react-redux';
import GameComingCyclone from './gameComing-Cyclone';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const gameComingContainerCyclone = connect(mapStateToProps, mapDispatchToProps)(GameComingCyclone);
export default gameComingContainerCyclone
