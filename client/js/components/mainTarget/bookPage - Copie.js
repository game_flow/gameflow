import React, {Component, PropTypes} from 'react';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/bookPage.css';
import { getLocalizedElement } from '../../util.js';
import GameDescription from './Elements/gameDescription.js';
import BookCollection from './Elements/BookCollection.js';


export default class BookPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer bookPageContainer">
                <div className="leftPart">
					<GameDescription language={this.props.language} name="firstAv-dragon"/> 
						{/*<div className="gameOutTttV textModule">
                        <iframe src={this.props.language["firstAv-tttv"]} width="550" height="250" frameBorder="0" allowFullScreen>
						</iframe>*/}

					<div className="Book3D textModule"> 
						<img  className="BookSplash" src={this.props.language["firstAv-dragon-book3d"]} />	
					</div>
                </div>
                <div className="rightPart">
					<div>
						<BookCollection  language={this.props.language} /> 
					</div>
					<div>
						<hr className="separation"/>
					</div>
					<div className="BookPartner textModule">
                        <h4>{this.props.language["firstAv-partners"]}</h4>
						{getLocalizedElement(this.props.language["firstAv-authors"])}
						<br/>
					</div>
                    <div className="BookPartner textModule">	
						<div className="BookPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["firstAv-partnerIllusLink"]} className="BookpartnerTextLink">
								{this.props.language["firstAv-partnerIllusName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["firstAv-partnerIllusLink"]}>
							<img 
								alt="test"
								src={this.props.language["firstAv-partnerIllusLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>		
                    </div>
					<div className="BookPartner textModule">		
						<div className="BookPartnersTextPart">
							{this.props.language["chimere-partnerDistribInfo"]}
							<br/>
							<a href={this.props.language["chimere-partnerDistribLink"]} className="BookpartnerTextLink">
								{this.props.language["chimere-partnerDistribName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["chimere-partnerDistribLink"]}>
							<img 
								src={this.props.language["chimere-partnerDistribLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>
					</div>		
					<div className="BookPartner textModule">		
						<div className="BookPartnersTextPart">
							{this.props.language["firstAv-partnerFabInfo"]}
							<br/>
							<a href={this.props.language["firstAv-partnerFabLink"]} className="BookpartnerTextLink">
								{this.props.language["firstAv-partnerFabName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["firstAv-partnerFabLink"]}>
							<img 
								src={this.props.language["firstAv-partnerFabLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>
					</div>	
				</div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

BookPage.propTypes = {
    language: PropTypes.object.isRequired
};