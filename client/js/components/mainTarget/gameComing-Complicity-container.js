import {connect} from 'react-redux';
import GameComingComplicity from './gameComing-Complicity';

const mapStateToProps = (state) => {
    return {
        language: state.language,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const gameComingContainerComplicity = connect(mapStateToProps, mapDispatchToProps)(GameComingComplicity);
export default gameComingContainerComplicity
