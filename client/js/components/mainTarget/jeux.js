import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/jeux.css';
import NewsLetter from './Elements/newsLetter.js';
import { getLocalizedElement } from '../../util.js';
import {Link} from 'react-router';

export default class Jeux extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer jeuxContainer">
                <div className="leftPart">

				   <div className="receptionListeJeux textModule">
						<h1>{getLocalizedElement(this.props.language["game-description"])}</h1>
						<div className="GameMini">
					<div className="Separate">
						<hr className="separation"/>
					</div>

          <div className="GameMini">
            <div className="GameMiniImageContainer">
              <Link to="/jungle">
              <img
              src={this.props.language["game-jungleMini"]}
              alt={this.props.language["game-jungleName"]}
              className="gameMiniLink" />
              </Link>
            </div>
            <div className="GameMiniTextContainer">
            <Link to="/jungle" className="receptionGameTextLink">
              {this.props.language["game-jungleName"]}
            </Link>
            <br/>
            <br/>
            <br/>
            {this.props.language["game-tocome02"]}
            </div>
          </div>
          <div className="Separate">
            <hr className="separation"/>
          </div>


          <div className="GameMini">
            <div className="GameMiniImageContainer">
              <Link to="/puzzleocre">
              <img
              src={this.props.language["game-puzzleocreMini"]}
              alt={this.props.language["game-puzzleocreName"]}
              className="gameMiniLink" />
              </Link>
            </div>
            <div className="GameMiniTextContainer">
            <Link to="/puzzleocre" className="receptionGameTextLink">
              {this.props.language["game-puzzleocreName"]}
            </Link>
            <br/>
            <br/>
            <br/>
            {this.props.language["game-tocome01"]}
            </div>
          </div>
          <div className="Separate">
            <hr className="separation"/>
          </div>

          <div className="GameMini">
            <div className="GameMiniImageContainer">
              <Link to="/argonuts">
              <img
              src={this.props.language["game-argonutsMini"]}
              alt={this.props.language["game-argonutsName"]}
              className="gameMiniLink" />
              </Link>
            </div>
            <div className="GameMiniTextContainer">
            <Link to="/argonuts" className="receptionGameTextLink">
              {this.props.language["game-argonutsName"]}
            </Link>
            <br/>
            <br/>
            <br/>
            {this.props.language["game-available"]}
            </div>
          </div>
          <div className="Separate">
            <hr className="separation"/>
          </div>


          <div className="GameMini">
            <div className="GameMiniImageContainer">
              <Link to="/puzzledragon">
              <img
              src={this.props.language["game-puzzledragonMini"]}
              alt={this.props.language["game-puzzledragonName"]}
              className="gameMiniLink" />
              </Link>
            </div>
            <div className="GameMiniTextContainer">
            <Link to="/puzzledragon" className="receptionGameTextLink">
              {this.props.language["game-puzzledragonName"]}
            </Link>
            <br/>
            <br/>
            <br/>
            {this.props.language["game-available"]}
            </div>
          </div>
          <div className="Separate">
            <hr className="separation"/>
          </div>

					<div className="GameMini">
					  <div className="GameMiniImageContainer">
						  <Link to="/bibliotheque">
						  <img
							src={this.props.language["game-bibliothequeMini"]}
							alt={this.props.language["game-bibliothequeName"]}
							className="gameMiniLink" />
						  </Link>
					  </div>
					  <div className="GameMiniTextContainer">
						<Link to="/bibliotheque" className="receptionGameTextLink">
						  {this.props.language["game-bibliothequeName"]}
						</Link>
						<br/>
						<br/>
						<br/>
						{this.props.language["game-available"]}
					  </div>
					</div>
					<div className="Separate">
					  <hr className="separation"/>
					</div>
						<div className="GameMiniImageContainer">
								<Link to="/dorsdragondor">
								<img
									src={this.props.language["game-cycloneMini"]}
									alt={this.props.language["game-cycloneName"]}
									className="gameMiniLink" />
								</Link>
						</div>
						<div className="GameMiniTextContainer">
							<Link to="/dorsdragondor" className="receptionGameTextLink">
								{this.props.language["game-cycloneName"]}
							</Link>
							<br/>
							<br/>
							<br/>
							{this.props.language["game-available"]}
						</div>
					</div>
					<div className="Separate">
						<hr className="separation"/>
					</div>
					<div className="GameMini">
					  <div className="GameMiniImageContainer">
						  <Link to="/5empereurs">
						  <img
							src={this.props.language["game-5empereursMini"]}
							alt={this.props.language["game-5empereursName"]}
							className="gameMiniLink" />
						  </Link>
					  </div>
					  <div className="GameMiniTextContainer">
						<Link to="/5empereurs" className="receptionGameTextLink">
						  {this.props.language["game-5empereursName"]}
						</Link>
						<br/>
						<br/>
						<br/>
						{this.props.language["game-available"]}
					  </div>
					</div>
					<div className="Separate">
					  <hr className="separation"/>
					</div>

					<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/dahu">
									<img
										src={this.props.language["game-dahuMini"]}
										alt={this.props.language["game-dahuName"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/dahu" className="receptionGameTextLink">
									{this.props.language["game-dahuName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
						<div className="Separate">
							<hr className="separation"/>
						</div>



						<div className="GameMini">
						  <div className="GameMiniImageContainer">
							  <Link to="/auvoloeuf">
							  <img
								alt={this.props.language["game-enqueteName"]}
								src={this.props.language["game-enqueteMini"]}
								className="gameMiniLink" />
							  </Link>
						  </div>
						  <div className="GameMiniTextContainer">
							<Link to="/auvoloeuf" className="receptionGameTextLink">
							  {this.props.language["game-enqueteName"]}
							</Link>
							<br/>
							<br/>
							<br/>
							{this.props.language["game-available"]}
						  </div>
						</div>
						<div className="Separate">
						  <hr className="separation"/>
						</div>

							<div className="GameMini">
								<div className="GameMiniImageContainer">
										<Link to="/champfleuri">
										<img
											alt={this.props.language["game-abeillesName"]}
											src={this.props.language["game-abeillesMini"]}
											className="gameMiniLink" />
										</Link>
								</div>
								<div className="GameMiniTextContainer">
									<Link to="/champfleuri" className="receptionGameTextLink">
										{this.props.language["game-abeillesName"]}
									</Link>
									<br/>
									<br/>
									<br/>
									{this.props.language["game-available"]}
								</div>
							</div>
							<div className="Separate">
								<hr className="separation"/>
							</div>
							<div className="GameMini">
								<div className="GameMiniImageContainer">
										<Link to="/cassetout">
										<img
											alt={this.props.language["game-raceName"]}
											src={this.props.language["game-raceMini"]}
											className="gameMiniLink" />
										</Link>
								</div>
								<div className="GameMiniTextContainer">
									<Link to="/cassetout" className="receptionGameTextLink">
										{this.props.language["game-raceName"]}
									</Link>
									<br/>
									<br/>
									<br/>
									{this.props.language["game-available"]}
								</div>
							</div>
							<div className="Separate">
								<hr className="separation"/>
							</div>
							<div className="GameMini">
								<div className="GameMiniImageContainer">
										<Link to="/ocre">
										<img
											alt={this.props.language["game-ocreName"]}
											src={this.props.language["game-ocreMini"]}
											className="gameMiniLink" />
										</Link>
								</div>
								<div className="GameMiniTextContainer">
									<Link to="/ocre" className="receptionGameTextLink">
										{this.props.language["game-ocreName"]}
									</Link>
									<br/>
									<br/>
									<br/>
									{this.props.language["game-available"]}
								</div>
							</div>
							<div className="Separate">
								<hr className="separation"/>
							</div>
							<div className="GameMini">
							<div className="GameMiniImageContainer">
								<Link to="/phobos">
									<img
										alt={this.props.language["game-phobosName"]}
										src={this.props.language["game-phobosMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/phobos" className="receptionGameTextLink">
									{this.props.language["game-phobosName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
						<div className="Separate">
							<hr className="separation"/>
						</div>
						<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/affinityExtension">
									<img
										alt={this.props.language["game-affinityExtName"]}
										src={this.props.language["game-affinityExtMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/affinityExtension" className="receptionGameTextLink">
									{this.props.language["game-affinityExtName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
						<div className="Separate">
							<hr className="separation"/>
						</div>
						<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/atlantide">
									<img
										alt={this.props.language["game-atlantideName"]}
										src={this.props.language["game-atlantideMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/atlantide" className="receptionGameTextLink">
									{this.props.language["game-atlantideName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
							<div className="Separate">
								<hr className="separation"/>
							</div>
							<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/premiereAventure">
									<img
										alt={this.props.language["game-firstAvName"]}
										src={this.props.language["game-firstAvMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/premiereAventure" className="receptionGameTextLink">
									{this.props.language["game-firstAvName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
						<div className="Separate">
							<hr className="separation"/>
						</div>
						<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/affinity">
									<img
										alt={this.props.language["game-complicityName"]}
										src={this.props.language["game-complicityMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/affinity" className="receptionGameTextLink">
									{this.props.language["game-complicityName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-available"]}
							</div>
						</div>
						<div className="Separate">
							<hr className="separation"/>
						</div>
						<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/chimere">
									<img
										alt={this.props.language["game-chimereName"]}
										src={this.props.language["game-chimereMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/chimere" className="receptionGameTextLink">
									{this.props.language["game-chimereName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-over"]}
							</div>
						</div>
						{/*<div className="Separate">
							<hr className="separation"/>
						</div>
						<div className="GameMini">
							<div className="GameMiniImageContainer">
									<Link to="/cyclone">
									<img
										alt={this.props.language["game-cycloneName"]}
										src={this.props.language["game-cycloneMini"]}
										className="gameMiniLink" />
									</Link>
							</div>
							<div className="GameMiniTextContainer">
								<Link to="/cyclone" className="receptionGameTextLink">
									{this.props.language["game-cycloneName"]}
								</Link>
								<br/>
								<br/>
								<br/>
								{this.props.language["game-tocome2019"]}
							</div>
						</div>
						*/}
					</div>
                </div>
                <div className="rightPart">

                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

Jeux.propTypes = {
    language: PropTypes.object.isRequired
};
