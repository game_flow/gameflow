import {connect} from 'react-redux';
import puzzleocrePage from './puzzleocrePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(puzzleocrePage);
export default bookPageContainer
