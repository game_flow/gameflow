import {connect} from 'react-redux';
import Contact from './contact';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const contactContainer = connect(mapStateToProps, mapDispatchToProps)(Contact);
export default contactContainer
