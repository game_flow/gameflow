import {connect} from 'react-redux';
import AbeillesPage from './abeillesPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(AbeillesPage);
export default bookPageContainer
