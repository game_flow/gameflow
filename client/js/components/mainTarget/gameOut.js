import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/gameOut.css';
import { getLocalizedElement } from '../../util.js';
import Partner from './Elements/partner.js';
import GameDescription from './Elements/gameDescription.js';

export default class GameOut extends Component {

    constructor(props) {
        super(props);
        this.state = {};
		this.name = this.props.name ;    
    }


    render() {
		const context = this;

        return (
            <div className="tabContainer gameOutContainer">
                <div className="leftPart">
                    <GameDescription language={this.props.language} name="chimere"/>
                    {/*<div className="gameOutPresse textModule">
                        Dossier de presse
                        Lien
                        => avoir si gestion news
                        Ou gestion au plus interessant
                        Ou si page à part un peu funky
                    </div>*/}
                    <div className="gameOutTttV textModule">
                        <iframe src={this.props.language["chimere-tttv"]} width="550" height="250" frameBorder="0" allowFullScreen>
						</iframe>
                    </div>
                </div>
                <div className="rightPart">
                    <div className="gameOutTttV textModule"> 
						<b>{this.props.language["chimere-LudoVox"]}</b>
						<iframe width="295" height="250" src={this.props.language["chimere-ludochrono"]} frameBorder="0" allowFullScreen></iframe>
                    </div>
                    <div className="gameOutDLRules textModule"> 
						<a href={this.props.language["chimere-rules"]} download="chimere.pdf" >
							<div className="gameOutDLRulesLeftPart">
								 
							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
								{this.props.language["chimere-downladRulesTitle"]}
								</p>
								<p className="pdfDLContent">
								{this.props.language["chimere-downladRules"]}
								</p>
							</div>
						</a>
                    </div>  
					<div className="gameOutGameInfos textModule"> 
						<img  className="gameOutSplash" src={this.props.language["chimere-gameInfos"]} />	
					</div>
                    <div className="gameOutBuy textModule">
						<a href={this.props.language["chimere-WhereBuyLink"]} className="WhereBuyLink">
							 <div className="WhereBuy">
								<p className="WhereBuyTitle">
								{this.props.language["chimere-WhereBuyTitle"]}
								</p>
								<p className="WhereBuyInfos">
								{this.props.language["chimere-WhereBuy"]}
								</p>
							</div>
						</a>
					</div>
                    <div className="gameOutPartner textModule">
                        <h4>{this.props.language["chimere-partners"]}</h4>
						
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerDistribInfo"]}
							<br/>
							<a href={this.props.language["chimere-partnerDistribLink"]} className="partnerTextLink">
								{this.props.language["chimere-partnerDistribName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["chimere-partnerDistribLink"]}>
							<img 
								src={this.props.language["chimere-partnerDistribLogo"]}
								className="partnerLink" />
							</a>
						</div>
					</div>
					<div className="gameOutPartner textModule">	
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["chimere-partnerIllusLink"]} className="partnerTextLink">
								{this.props.language["chimere-partnerIllusName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["chimere-partnerIllusLink"]}>
							<img 
								alt="test"
								src={this.props.language["chimere-partnerIllusLogo"]}
								className="partnerLink" />
							</a>
						</div>		
                    </div>
					<div className="gameOutPartner textModule">	
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerPackInfo"]}
							<br/>
							<a href={this.props.language["chimere-partnerPackLink"]} className="partnerTextLink">
								{this.props.language["chimere-partnerPackName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["chimere-partnerPackLink"]}>
							<img 
								alt="test"
								src={this.props.language["chimere-partnerPackLogo"]}
								className="partnerLink" />
							</a>
						</div>		
                    </div>                
                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property [name] - name of the game.
*/

GameOut.propTypes = {
    language: PropTypes.object.isRequired,
	name : PropTypes.string,
	
	
};