import {connect} from 'react-redux';
import puzzledragonPage from './puzzledragonPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(puzzledragonPage);
export default bookPageContainer
