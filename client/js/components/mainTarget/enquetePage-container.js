import {connect} from 'react-redux';
import enquetePage from './enquetePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(enquetePage);
export default bookPageContainer
