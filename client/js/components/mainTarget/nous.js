import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/nous.css';
import { getLocalizedElement } from '../../util.js';

export default class Nous extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer">
                <div className="leftPart nousContainer">
                    <div className="nousGF textModule">
                        <h3> {this.props.language["us-TitleGameFlow"]}</h3>
						{getLocalizedElement(this.props.language["us-GameFlowDescription"])}
                    </div>
                    {/*<div className="nousPresseGF textModule">
                        Presse GF
                    </div>
                    <div className="nousNous textModule">
                         <h3> {this.props.language["us-TitleWhoWeAre"]} </h3>
						 {getLocalizedElement(this.props.language["us-CLDescription"])}
						 {getLocalizedElement(this.props.language["us-RHDescription"])}
					</div>*/}
					<div className="nousNousTitle textModule">
                        <h3>{this.props.language["us-TitleWhoWeAre"]}</h3>
					</div>
					<div className="nousNous textModule">
						<div className="NousTextPart">
							<b>{this.props.language["us-CLTitle"]}</b>
							<br/>
							<i>{this.props.language["us-CLSubtitle"]}</i>
							<br/>
							<hr className="separation"/>
							<br/>
							{getLocalizedElement(this.props.language["us-CLDescription"])}
						</div>
						<div className="NousPortraitsContainer">
							<img
								alt={this.props.language["us-CLTitle"]}
								src={this.props.language["us-CLFile"]}
								className="NousPortraits" />
						</div>
					</div>
					<div className="nousNous textModule">
						<div className="NousPortraitsContainer">
							<img
								alt={this.props.language["us-RHTitle"]}
								src={this.props.language["us-RHFile"]}
								className="NousPortraits" />
						</div>
						<div className="NousTextPart">
							<b>{this.props.language["us-RHTitle"]}</b>
							<br/>
							<i>{this.props.language["us-RHSubtitle"]}</i>
							<br/>
							<hr className="separation"/>
							<br/>
							{getLocalizedElement(this.props.language["us-RHDescription"])}
						</div>
					</div>

          <div className="nousNous textModule">
						<div className="NousTextPart">
							<b>{this.props.language["us-GBTitle"]}</b>
							<br/>
							<i>{this.props.language["us-GBSubtitle"]}</i>
							<br/>
							<hr className="separation"/>
							<br/>
							{getLocalizedElement(this.props.language["us-GBDescription"])}
						</div>
						<div className="NousPortraitsContainer">
							<img
								alt={this.props.language["us-GBTitle"]}
								src={this.props.language["us-GBFile"]}
								className="NousPortraits" />
						</div>
					</div>

          <div className="nousNous textModule">
						<div className="NousPortraitsContainer">
							<img
								alt={this.props.language["us-XBTitle"]}
								src={this.props.language["us-XBFile"]}
								className="NousPortraits" />
						</div>
						<div className="NousTextPart">
							<b>{this.props.language["us-XBTitle"]}</b>
							<br/>
							<i>{this.props.language["us-XBSubtitle"]}</i>
							<br/>
							<hr className="separation"/>
							<br/>
							{getLocalizedElement(this.props.language["us-XBDescription"])}
						</div>
					</div>

          <div className="nousNous textModule">
            <div className="NousTextPart">
              <b>{this.props.language["us-SBTitle"]}</b>
              <br/>
              <i>{this.props.language["us-SBSubtitle"]}</i>
              <br/>
              <hr className="separation"/>
              <br/>
              {getLocalizedElement(this.props.language["us-SBDescription"])}
            </div>
            <div className="NousPortraitsContainer">
              <img
                alt={this.props.language["us-SBTitle"]}
                src={this.props.language["us-SBFile"]}
                className="NousPortraits" />
            </div>
          </div>

          </div>

                <div className="rightPart">
                     <div className="nousPartnerContainer textModule">
                        <h4>{this.props.language["us-partners"]}</h4>
						<div className="nousPartners textModule" >
							<div className="nousPartnersTextPart">
								<a href={this.props.language["us-partnerGAIALink"]} className="nousTextLink">
									{this.props.language["us-partnerGAIAName"]}
								</a>
								<br/>
								{getLocalizedElement(this.props.language["us-partnerGAIAInfo"])}
							</div>
							<div className="nousPartnerLogo">
								<a href={this.props.language["us-partnerGAIALink"]}>
								<img
									alt="Gaia"
									src={this.props.language["us-partnerGAIALogo"]}
									className="nousLink" />
								</a>
							</div>
						</div>
						<div className="nousPartners textModule" >
							<div className="nousPartnerLogo">
								<a href={this.props.language["us-partnerKFeeLink"]}>
								<img
									alt="Gaia"
									src={this.props.language["us-partnerKFeeLogo"]}
									className="nousLink" />
								</a>
							</div>
							<div className="nousPartnersTextPart">
								<a href={this.props.language["us-partnerKFeeLink"]} className="nousTextLink">
									{this.props.language["us-partnerKFeeName"]}
								</a>
								<br/>
								{getLocalizedElement(this.props.language["us-partnerKFeeInfo"])}
							</div>
						</div>
						<div className="nousPartners textModule" >
							<div className="nousPartnersTextPart">
								<a href={this.props.language["us-partnerLaPousadaLink"]} className="nousTextLink">
									{this.props.language["us-partnerLaPousadaName"]}
								</a>
								<br/>
								{getLocalizedElement(this.props.language["us-partnerLaPousadaInfo"])}
							</div>
							<div className="nousPartnerLogo">
								<a href={this.props.language["us-partnerLaPousadaLink"]}>
								<img
									alt="La Pousada"
									src={this.props.language["us-partnerLaPousadaLogo"]}
									className="nousLink" />
								</a>
							</div>
						</div>
					</div>
					<hr className="separation"/>
					<div>
					<h4>{getLocalizedElement(this.props.language["us-contact"])}		</h4>
					</div>
					<div className="nousContact textModule">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Clem"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-ClemInfo"])}
						</div>
					</div>
					<div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Rom"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-RomInfo"])}
						</div>
					</div>
					<div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Gaet"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-GaetInfo"])}
						</div>
					</div>
          <div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Xav"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-XavInfo"])}
						</div>
					</div>
					<div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Margot"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-MargotInfo"])}
						</div>
					</div>
					<div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Caro"])}
						</div>
						<div className="nousContactRight">
							{getLocalizedElement(this.props.language["contact-CaroInfo"])}
						</div>
					</div>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<div className="nousContact">
						<div className="nousContactLeft">
							{getLocalizedElement(this.props.language["contact-Adress"])}
						</div>
						<div className="nousAdressRight">
							{getLocalizedElement(this.props.language["contact-Adresse"])}
						</div>
					</div>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<div>
					<hr className="separation"/>
					</div>
					<div>
						<h4>{getLocalizedElement(this.props.language["us-specialThanks"])}		</h4>
					</div>
                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

Nous.propTypes = {
    language: PropTypes.object.isRequired
};
