import {connect} from 'react-redux';
import Reception from './reception';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const receptionContainer = connect(mapStateToProps, mapDispatchToProps)(Reception);
export default receptionContainer
