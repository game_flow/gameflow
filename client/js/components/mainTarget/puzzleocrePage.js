import React, {Component, PropTypes} from 'react';

// Pour le caroussel
import {Carousel} from 'react-responsive-carousel';
// general styles
//import 'style!css!react-responsive-carousel/lib/styles/main.css';
import '../../../../node_modules/react-responsive-carousel/lib/styles/main.css';
// carousel styles
import '../../../../node_modules/react-responsive-carousel/lib/styles/carousel.css';

import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/bookPage.css';
import { getLocalizedElement } from '../../util.js';
import MPZDescription from './Elements/MPZDescription.js';
import MPZCollection from './Elements/MPZCollection.js';


export default class puzzleocrePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer puzzleocreContainer">
                <div className="leftPart">
					<MPZDescription language={this.props.language} name="MPZ-puzzleocre"/>
                </div>
                <div className="rightPart">
					<div>
						<MPZCollection  language={this.props.language} />
					</div>
					<div>
						<hr className="separation"/>
					</div>
					<div className="BookPartner textModule">
                        <h4>{this.props.language["MPZ-partners"]}</h4>
						{getLocalizedElement(this.props.language["MPZ-puzzleocre-authors"])}
						<br/>
					</div>

                    <div className="BookPartner textModule">
						<div className="BookPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["MPZ-puzzleocre-partnerIllusLink"]} className="BookpartnerTextLink">
								{this.props.language["MPZ-puzzleocre-partnerIllusName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["MPZ-puzzleocre-partnerIllusLink"]}>
							<img
								alt="test"
								src={this.props.language["MPZ-puzzleocre-partnerIllusLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>
                    </div>
					<div className="BookPartner textModule">
						<div className="BookPartnersTextPart">
							{this.props.language["chimere-partnerDistribInfo"]}
							<br/>
							<a href={this.props.language["chimere-partnerDistribLink"]} className="BookpartnerTextLink">
								{this.props.language["chimere-partnerDistribName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["chimere-partnerDistribLink"]}>
							<img
								src={this.props.language["chimere-partnerDistribLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>
					</div>
					<div className="BookPartner textModule">
						<div className="BookPartnersTextPart">
							{this.props.language["MPZ-puzzleocre-partnerFabInfo"]}
							<br/>
							<a href={this.props.language["MPZ-puzzleocre-partnerFabLink"]} className="BookpartnerTextLink">
								{this.props.language["MPZ-puzzleocre-partnerFabName"]}
							</a>
						</div>
						<div className="BookPartnerLogo">
							<a href={this.props.language["MPZ-puzzleocre-partnerFabLink"]}>
							<img
								src={this.props.language["MPZ-puzzleocre-partnerFabLogo"]}
								className="BookpartnerLink" />
							</a>
						</div>
					</div>


				</div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

puzzleocrePage.propTypes = {
    language: PropTypes.object.isRequired
};
