import {connect} from 'react-redux';
import argonutsPage from './argonutsPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(argonutsPage);
export default bookPageContainer
