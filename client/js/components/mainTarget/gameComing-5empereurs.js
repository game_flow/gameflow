import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {sortBy, concat, forEach, isEqual} from 'lodash';
import {Icon} from 'react-fa';
import '../../../css/gameComing.css';
import { getLocalizedElement } from '../../util.js';
import GameDescription from './Elements/gameDescription.js';

export default class GameComing5empereurs extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="tabContainer gameComingContainer-cy">
                <div className="leftPart">
					<GameDescription language={this.props.language} name="5empereurs"/>
					
                </div>
                <div className="rightPart">
					<div className="gameOutTttV textModule">
						<b>{this.props.language["5empereurs-TrailerText"]}</b>
						<iframe width="295" height="250" src={this.props.language["5empereurs-Trailer"]} frameBorder="0" allowFullScreen></iframe>
                    </div>
					<div className="gameOutDLRules textModule">
						<a href={this.props.language["5empereurs-rules"]} download="5Empereurs_Rules_Light.pdf" >
							<div className="gameOutDLRulesLeftPart">

							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
								{this.props.language["5empereurs-downladRulesTitle"]}
								</p>
								<p className="pdfDLContent">
								{this.props.language["5empereurs-downladRules"]}
								</p>
							</div>
						</a>
                    </div>
					<div className="gameOutDLRules textModule">
						<a href={this.props.language["5empereurs-historicBook"]} download="5EMP_LivretHistorique_Light.pdf" >
							<div className="gameOutDLRulesLeftPart">

							</div>
							<div className="gameOutDLRulesRightPart">
								<p className="pdfDLTitle">
								{this.props.language["5empereurs-downladHistoBookTitle"]}
								</p>
								<p className="pdfDLContent">
								{this.props.language["5empereurs-downladHistoBook"]}
								</p>
							</div>
						</a>
                    </div>
					<div className="gameComingGameInfos textModule">
						<img  className="gameComingSplash" src={this.props.language["5empereurs-gameInfos"]} />
					</div>
					<div className="gameOutBuy textModule">
						<a href={this.props.language["chimere-WhereBuyLink"]} className="WhereBuyLink">
							 <div className="WhereBuy">
								<p className="WhereBuyTitle">
								{this.props.language["chimere-WhereBuyTitle"]}
								</p>
								<p className="WhereBuyInfos">
								{this.props.language["chimere-WhereBuy"]}
								</p>
							</div>
						</a>
					</div>
					<div className="gameOutPartner textModule">
						<h4>{this.props.language["5empereurs-partners"]}</h4>
						{getLocalizedElement(this.props.language["5empereurs-authors"])}
						<br/>
					</div>

					<div className="gameOutPartner textModule">
						<div className="gameOutPartnersTextPart">
							{this.props.language["chimere-partnerIllusInfo"]}
							<br/>
							<a href={this.props.language["5empereurs-partnerIllusLink"]} className="partnerTextLink">
								{this.props.language["5empereurs-partnerIllusName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["5empereurs-partnerIllusLink"]}>
							<img
								alt="test"
								src={this.props.language["5empereurs-partnerIllusLogo"]}
								className="partnerLink" />
							</a>
						</div>
					</div>
					<div className="gameOutPartner textModule">
						<div className="gameOutPartnersTextPart">
							{this.props.language["5empereurs-partnerDistribInfo"]}
							<br/>
							<a href={this.props.language["5empereurs-partnerDistribLink"]} className="partnerTextLink">
							{this.props.language["5empereurs-partnerDistribName"]}
							</a>
						</div>
						<div className="gameOutPartnerLogo">
							<a href={this.props.language["5empereurs-partnerDistribLink"]}>
							<img
								src={this.props.language["5empereurs-partnerDistribLogo"]}
								className="partnerLink" />
							</a>
						</div>
					</div>
					<br/>
					<br/>
					<br/>
					<div className="gameComingGameInfos textModule"> 
						<img  className="gameOutImg" src={this.props.language["5empereurs-Img"]} />	
					</div>
                </div>
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
*/

GameComing5empereurs.propTypes = {
    language: PropTypes.object.isRequired
};
