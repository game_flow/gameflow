import {connect} from 'react-redux';
import GameComing5empereurs from './gameComing-5empereurs';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const gameComingContainer5empereurs = connect(mapStateToProps, mapDispatchToProps)(GameComing5empereurs);
export default gameComingContainer5empereurs
