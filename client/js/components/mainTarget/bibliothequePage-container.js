import {connect} from 'react-redux';
import bibliothequePage from './bibliothequePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(bibliothequePage);
export default bookPageContainer
