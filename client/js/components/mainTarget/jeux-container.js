import {connect} from 'react-redux';
import Jeux from './jeux';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const contactContainer = connect(mapStateToProps, mapDispatchToProps)(Jeux);
export default contactContainer
