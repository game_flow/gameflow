import {connect} from 'react-redux';
import dahuPage from './dahuPage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(dahuPage);
export default bookPageContainer
