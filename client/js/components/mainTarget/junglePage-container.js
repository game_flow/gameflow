import {connect} from 'react-redux';
import junglePage from './junglePage';

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const bookPageContainer = connect(mapStateToProps, mapDispatchToProps)(junglePage);
export default bookPageContainer
