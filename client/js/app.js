import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import {browserHistory} from './appConfig';
import RouterContainer from './routing/router-container';

import '../css/app.css';

const middlewares = [routerMiddleware(browserHistory), thunk];

let defaultStore = {
    language: {},
    routing:{},
    main:{
        bannerClass:"bannerContainer",
        refreshItems:false, // this is used to force caroussel items repaint (otherwise they tend to disappear)
        popupInfo:{
            item:{},
            posterImagePath:"",
            posterlocation:{height:0, width:0, top:0, left:0},
        },
        carouselItems:[
            {
                name:"Octogone 2019",
                imageSource:"Octogon2010.png",
                link:"https://www.octogones.org/programme/",
                date:"Du 4 au 6 octobre",
                timestamp:1570197600000,
                size:2,
                text:"<br> Convention Octogone à Lyon <br> Avec Roméo et Etienne",
                type:"salon",
                highlight:false
            },
            {
                name:"Essen 2019",
                imageSource:"Essen.png",
                link:"https://www.spiel-messe.com",
                date:"Du 24 au 27 octobre",
                timestamp:1571911200000,
                size:2,
                text:"<br> Salon d'Essen (Allemagne) <Br> ",
                type:"essen",
                highlight:false
            },
            {
                name:"Montbéliard 2019",
                imageSource:"montbeliard_2019.jpg",
                link:"fvj.la-croisee-des-jeux.asso.fr/",
                date:"9 et 10 novembre",
                timestamp:1573293600000,
                size:2,
                text:"<br> Festival de Montbéliard <br> Avec Margot !",
                type:"salon",
                bigHoverZone:true,
                highlight:false
            },
            {
                name:"Ugine 2019",
                imageSource:"Affiche_Festival_Jeu_Jouet_2019.jpg",
                link:"https://www.ugine.com/festival-du-jeu-et-jouet/",
                date:"7 et 8 décembre",
                timestamp:1575727200000,
                size:2,
                text:"<br> Salon du jeu et du Jouet d'Ugine <br> Avec Roméo, Clément et Etienne",
                type:"salon",
                highlight:false
            },
            {
                name:"Cannes 2020",
                imageSource:"Cannes.jpg",
                link:"https://www.festivaldesjeux-cannes.com/",
                date:"du 21 au 23 février",
                timestamp:1582279200000,
                size:2,
                text:"Festival des jeux de Cannes <br> Stand GameFlow dans la zone BlackRock. <br> Et.. tout le monde sera là!",
                type:"cannes",
                highlight:false
            },
			{
                name:"MJC Montchat",
                imageSource:"MJC.jpg",
                link:"https://www.facebook.com/events/754729401653445/",
                date:"21 Novembre à 18h",
                timestamp:1574359200000,
                size:2,
                text:"<br> Soirée à la MJC de Monchat (Lyon)",
                type:"soiree",
                highlight:false
            },
			{
                name:"Dédicace Cultura",
                imageSource:"",
                link:"",
                date:"11 Décembre à 14h",
                timestamp:1576072845000,
                size:2,
                text:"Séance de dédicaces <br> au <b>Cultura</b> de Bourgoin-Jallieu <br> Avec <b>Roméo Hennion</b> et <br> <b>Arnaud Boutle</b>",
                type:"dedicace",
                highlight:false
            },
			{
                name:"Soirée Affinity",
                imageSource:"Philibar.jpg",
                link:"https://www.facebook.com/events/537097840354365/",
                date:"12 Décembre à 19h",
                timestamp:1576177245000,
                size:2,
                text:"Soirée <b>Affinity</b> <br> au <b>Philibar</b> à Strasbourg <br> Avec Margot",
                type:"soiree",
                highlight:false
            },
			{
                name:"Sortie Livre 2",
                imageSource:"../../GameOut/atlantide/Miniatures_FR.jpg",
                link:"http://www.game-flow.fr/atlantide",
                date:"22 Février 2019",
                timestamp:1550831618000,
                size:2,
                text:"<br> Sortie de <i><b>La Découverte de l'Atlantide</i></b> <br> 2ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"Sortie Livre 3",
                imageSource:"../../GameOut/phobos/Miniatures_FR.jpg",
                link:"http://www.game-flow.fr/phobos",
                date:"27 septembre 2019",
                timestamp:1569576345000,
                size:2,
                text:"<br> Sortie de <i><b>L'Odyssée du Phobos</i></b> <br> 3ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"Sortie Livre 4",
                imageSource:"../../GameOut/ocre/miniatures.jpg",
                link:"http://www.game-flow.fr/ocre",
                date:"28 février 2020",
                timestamp:1582881945000,
                size:2,
                text:"<br> Sortie de <i><b>Voyage en terre Ocre</i></b> <br> 4ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"Sortie Chimere",
                imageSource:"../../GameOut/chimere/miniatures.png",
                link:"http://www.game-flow.fr/chimere",
                date:"25 juin 2016",
                timestamp:1466847881000,
                size:2,
                text:"<br> Sortie de <i><b>Chimère</i></b> <br> Notre premier jeu !! <br>",
                type:"sortieChimere",
                highlight:false
            },
			{
                name:"Sortie Affinity",
                imageSource:"../../GameOut/affinity/Miniatures.jpg",
                link:"http://www.game-flow.fr/affinity",
                date:"14 septembre 2018",
                timestamp:1536918281000,
                size:2,
                text:"<br> Sortie d'<i><b>Affinity</i></b> <br> Notre second jeu, plein de poésie ! <br>",
                type:"sortieAffinity",
                highlight:false
            },
			{
                name:"Sortie Affinity Sensuelle",
                imageSource:"../../GameOut/affinitySensuelle/miniatures.jpg",
                link:"http://www.game-flow.fr/affinityExtension",
                date:"05 juillet 2019",
                timestamp:1562319881000,
                size:2,
                text:"<br> Sortie de l'<i><b>Extension Sensuelle</i></b> <br> pour notre second jeu <b> Affinity</b> <br>",
                type:"sortieSensuel",
                highlight:false
            },
			{
                name:"Nuremberg Toy Fair 2020",
                imageSource:"Nuremberg.jpg",
                link:"https://www.spielwarenmesse.de/language/1/",
                date:"du 29 au 31 janvier",
                timestamp:1580199497000 ,
                size:2,
                text:"<br>Salon du jouet de Nuremberg (Allemagne).<br> Rendez-vous professionels.",
                type:"nuremberg",
                highlight:false
            },
			{
                name:"Epinal 2020",
                imageSource:"Epinal2020.jpg",
                link:"https://jeux-et-cie.fr/",
                date:"du 6 au 8 mars 2020",
                timestamp:1583569097000 ,
                size:2,
                text:"<br>Salon d'Epinal. <br> Un stand Game Flow <br> avec Margot !",
                type:"epinal",
                highlight:false
            },
			{
                name:"Domaine des Jeux 2020",
                imageSource:"Domaine2020.jpg",
                link:"https://www.lejoueurarverne.fr/festival-le-domaine-des-jeux/?fbclid=IwAR3zME9gKO6V5OAU4sNucbYQBCUMfryvSPfa_E7goi0KxH60k7pw2rmjeDs",
                date:"14 et 15 mars 2020",
                timestamp:1583569097000 ,
                size:2,
                text:"<br>Festival \"Le Domaine des jeux\" <br> Le festival est annulé !",
                type:"salon",
                highlight:false
            },
			{
                name:"Place aux Jeux 2020",
                imageSource:"PlaceAuxJeux.jpg",
                link:"http://placeauxjeux-grenoble.org/",
                date:"28 et 29 mars 2020",
                timestamp:1585390697000 ,
                size:2,
                text:"Festival \"Place Aux jeux\" <br> à Grenoble <br> Le festival est annulé !",
                type:"grenoble",
                highlight:false
            },
			{
                name:"DBDJ 2020",
                imageSource:"DBDJ2020.png",
                link:"https://desbretzelsetdesjeux.fr",
                date:"6 et 7 juin 2020",
                timestamp:1591438697000 ,
                size:2,
                text:"Des Bretzels et Des Jeux <br> à Strasbourg <br> Le festival est aussi annulé ...",
                type:"dbdj",
                highlight:false
            },
			{
                name:"PEL 2020",
                imageSource:"PEL2020.jpg",
                link:"https://sites.google.com/parisestludique.fr/pel2020/PEL?authuser=0",
                date:"27 et 28 juin 2020",
                timestamp:1593253097000 ,
                size:2,
                text:"Paris Est Ludique à Paris <br><br> Stand Game Flow,<br> Festival annulé...Vivement 2021!",
                type:"pel",
                highlight:false
            },
			{
                name:"FLIP 2020",
                imageSource:"FLIP2020.jpg",
                link:"http://www.jeux-festival.com",
                date:"8 et 19 juillet 2020",
                timestamp:1594203497000 ,
                size:2,
                text:"FLIP à Parthenay <br> Malheureusement...<br> c'est annulé !<br>",
                type:"flip",
                highlight:false
            },
			{
                name:"Vichy 2020",
                imageSource:"Vichy-2020.jpg",
                link:"https://www.ville-vichy.fr/festival-des-jeux",
                date:"26 et 27 septembre 2020",
                timestamp:1601121600000 ,
                size:2,
                text:"Festival de Vichy <br> Roméo et Clément seront présents <br> pour la partie publique et pro !",
                type:"salon",
                highlight:false
            },
			{
                name:"Sortie MPA5",
                imageSource:"../../GameOut/course/Book_3D_FR.png",
                link:"https://www.game-flow.fr/race",
                date:"11 Décembre 2020",
				timestamp:1607683991000,
                size:2,
                text:"<br><i>Sortie de <b>La Course des Casse-Tout</i></b><br> 5ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"Sortie MPA6",
                imageSource:"../../GameOut/abeilles/Book_3D_FR.png",
                link:"https://www.game-flow.fr/champfleuri",
                date:"28 mai 2021",
				timestamp:1622202107000,
                size:2,
                text:"<br><i> Sortie de <b>La Reine de Champ-Fleuri</i></b> <br> 6ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"Alchimie 2021",
                imageSource:"Alchimie_2021.jpg",
                link:"https://alchimiedujeu.fr/",
                date:"du 29 au 30 mai",
                timestamp:1622288507000,
                size:2,
                text:"<i>Le festival \"Alchimie du jeu\" de Toulouse</i><br>  Snif ! Le festival est annulé : \(",
                type:"salon",
                highlight:false
            },
      {
                name:"Ludinam",
                imageSource:"Ludinam2021.jpg",
                link:"https://www.facebook.com/ludinam",
                date:"du 07 au 09 mai",
                timestamp:1620391705000,
                size:2,
                text:"<i>Le festival \"Ludinam\" de Besançon</i><br><br>  Snif ! Le festival est annulé : \(",
                type:"salon",
                highlight:false
            },
      {
                name:"Paris est Ludique !",
                imageSource:"PEL2021.jpg",
                link:"https://www.parisestludique.fr/",
                date:"du 26 au 27 juin",
                timestamp:1624711705000,
                size:2,
                text:"<i>Le festival \"Paris est Ludique !\" de Paris</i><br>  Snif ! Le festival est annulé : \(",
                type:"pel",
                highlight:false
            },
			 {
                name:"Ludicité",
                imageSource:"Ludicite.jpg",
                link:"http://ludicite.lamad.net/",
                date:"le 26 juin",
                timestamp:1624711704000,
                size:2,
                text:"<i>\"Ludicité\" à Paris</i><br>  Clément fera découvrir <br> Ma Première Aventure et Affinity",
                type:"salon",
                highlight:false
            },
      {
                name:"Flip",
                imageSource:"FLIP2021.jpg",
                link:"http://www.jeux-festival.com/",
                date:"du 07 au 18 juillet",
                timestamp:1625662105000,
                size:2,
                text:"<i>Le Festival Ludique International de Parthenay</i><br>  Si le festival est maintenu <br> Margot y sera !",
                type:"flip",
                highlight:false
            },
      {
                name:"spielwarenmesse",
                imageSource:"Nurenberg2021.jpg",
                link:"https://www.spielwarenmesse.de/en",
                date:"du 20 au 24 juillet",
                timestamp:1626785305000,
                size:2,
                text:"<i>\"Spielwarenmesse\", le salon du jouet de Nuremberg</i><br><br>  Snif ! Le festival est annulé : \(",
                type:"nuremberg",
                highlight:false
            },
      {
                name:"Chamboultout",
                imageSource:"Chamboultou2021.png",
                link:"https://www.facebook.com/chamboultou19/",
                date:"du 21 au 22 août",
                timestamp:1629550105000,
                size:2,
                text:"<i>Le festival \"Chamboultou\" en Corrèze </i><br><br>  Nous ne pourrons pas participer à ce festival !",
                type:"salon",
                highlight:false
              },
        {
                name:"Vichy",
                imageSource:"Vichy2021.jpg",
                link:"https://subverti.com/fr/maps/festivals/festival-jeux-vichy/",
                date:"du 18 au 19 septembre",
                timestamp:1631969305000,
                size:2,
                text:"<i>Le festival des jeux à Vichy </i><br><br>  Avec Roméo, Clément, et Gaëtan !",
                type:"salon",
                highlight:false
              },
          {
                name:"SPIEL",
                imageSource:"SPIEL2021.jpg",
                link:"https://www.spiel-messe.com/en/",
                date:"du 14 au 17 octobre",
                timestamp:1634215705000,
                size:2,
                text:"<i>Le salon \"SPIEL\" à Essen </i><br><br>  Nous ne pourrons, hélas,<br> pas nous y rendre!",
                type:"essen",
                highlight:false
           },
		   {
                name:"Sortie MPA7",
                imageSource:"../../GameOut/enquete/Book_3D_FR.png",
                link:"https://www.game-flow.fr/auvoloeuf",
                date:"décembre 2021",
			         	timestamp:1639651784000,
                size:2,
                text:"<br><i> Sortie de <b>Au Vol-Oeuf!</i></b> <br> 7ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
                name:"ValJeux",
                imageSource:"Valjeux.jpg",
                link:"http://www.levaldesjeux.com/",
                date:"du 20 au 21 novembre",
                timestamp:1637400364000,
                size:2,
                text:"<i>Le festival \"Val des jeux\" à Serris </i><br><br>  Nous ne pourrons, hélas,<br> pas nous y rendre !",
                type:"salon",
                highlight:false
             },

      {
                name:"Ludimania",
                imageSource:"Ludimania2021.jpg",
                link:"http://www.levaldesjeux.com/",
                date:"du 11 au 12 septembre",
                timestamp:1631361479000,
                size:2,
                text:"<i>Le festival \"Ludimania\" vers Dijon </i><br><br>  Avec Jean-Philippe Sahut, Clément, et Gaëtan !",
                type:"salon",
                highlight:false
              },

        {
                name:"OctoGônes",
                imageSource:"Octogones2021.png",
                link:"https://www.octogones.org/",
                date:"du 01 au 03 octobre",
                timestamp:1633094783000,
                size:2,
                text:"<i>Le festival \"OctoGônes\" à Lyon Villeurbanne </i><br><br>  Avec Roméo & Gaëtan !",
                type:"salon",
                highlight:false
        },
        {
                name:"Des Livres & Vous",
                imageSource:"DESLIVRES&VOUS2022.jpg",
                link:"https://blanchepasquier.wixsite.com/salondeslivresetvous",
                date:"le 16 janvier",
                timestamp:1642330184000,
                size:2,
                text:"<i>Le festival \"Des Livres & Vous\" à la Vilette d'Anthon </i><br>  Avec Clément, Elodie Bénard, et Jean-Philippe Sahut !",
                type:"salon",
                highlight:false
        },
        {
                  name:"Spielwarenmesse 2022",
                  imageSource:"Nurenberg2021.jpg",
                  link:"https://www.spielwarenmesse.de/en/languages/french",
                  date:"du 2 au 6 février",
                  timestamp:1643798984000 ,
                  size:2,
                  text:"<i>Salon du jouet de Nuremberg (Allemagne)</i><br><br> Rendez-vous professionnels.",
                  type:"nuremberg",
                  highlight:false
          },
          {
              name:"FIJ 2022",
              imageSource:"FIJ2022.jpg",
              link:"https://www.festivaldesjeux-cannes.com/fr/festival/all",
              date:"du 25 au 27 février",
              timestamp:1645786184000,
              size:2,
              text:"Festival international des jeux de Cannes <br> Avec toute l'équipe de Game Flow !",
              type:"cannes",
              highlight:false
          }
		  ,
          {
              name:"Epinal 2022",
              imageSource:"JeuxCie.jpg",
              link:"https://jeux-et-cie.fr/",
              date:"du 11 au 13 mars",
              timestamp:1647009961000,
              size:2,
              text:"Festival Jeux et Compagnie <Br> à Epinal <br> Avec Margot !",
              type:"epinal",
              highlight:false
          }
      ,
          {
              name:"La Contrée des Jeux",
              imageSource:"LaContreeDesJeux2022.jpg",
              link:"https://www.facebook.com/lacontreedesjeux/",
              date:"du 21 au 22 mai 2022",
              timestamp:1653124641000,
              size:2,
              text:"Le festival des jeux d'Avignon. <br>Nous ne pourrons pas être présents, mais des jeux de chez nous seront dispo ! ",
              type:"salon",
              highlight:false
          }
      ,
          {
              name:"La fête du jeu à Biarritz",
              imageSource:"FeteDuJeuLudo64_2022.jpg",
              link:"https://www.facebook.com/laludo64",
              date:"le samedi 28 mai 2022",
              timestamp:1653729441000,
              size:2,
              text:"La fête du jeu à Biarritz. <br>Nous ne pourrons pas être présents, mais des jeux de chez nous seront dispo ! ",
              type:"salon",
              highlight:false
          }
      ,
          {
              name:"Le salon du jeu éducatif",
              imageSource:"Canope21_2022.png",
              link:"https://www.reseau-canope.fr/service/salon-du-jeu-educatif_23954.html",
              date:"le mercredi 8 juin 2022",
              timestamp:1654679841000,
              size:2,
              text:"L’atelier Canopé 21 - Dijon organise son deuxième Salon du jeu éducatif. Mathilde Malburet, Clément et Gaëtan seront présents ! ",
              type:"salon",
              highlight:false
          }
      ,
          {
              name:"Sortie MPA8",
              imageSource:"../../GameOut/dahu/Book_3D_FR.png",
              link:"https://ma-premiere-aventure.fr/livres/sur-la-piste-du-dahu",
              date:"Début juillet 2022",
              timestamp:1656667541000,
              size:2,
              text:"<br><i> Sortie de <b>Sur La Piste du Dahu</i></b> <br> 8ème livre de la gamme <br> Ma Première Aventure",
              type:"sortieMPA",
              highlight:false
          }
      ,
          {
            name:"PEL 2022",
            imageSource:"PEL2021.jpg",
            link:"https://www.parisestludique.fr/",
            date:"du 02 et 03 juillet 2022",
            timestamp:1656753441000 ,
            size:2,
            text:"<br>Paris Est Ludique à Paris <br><br> Nous y serons !",
            type:"pel",
            highlight:false
          }
      ,
          {
            name:"FLIP 2022",
            imageSource:"FLIP2022.jpg",
            link:"http://www.jeux-festival.com",
            date:"du 13 et 24 juillet 2022",
            timestamp:1657703841000 ,
            size:2,
            text:"<i>Le Festival Ludique International de Parthenay</i><br> Margot y sera !",
            type:"flip",
            highlight:false
          }
      ,
          {
            name:"Vichy",
            imageSource:"Vichy2022.png",
            link:"https://m.facebook.com/Festival-des-Jeux-%C3%A0-Vichy-1979003072349967/",
            date:"du 17 au 19 septembre 2022",
            timestamp:1662649218000,
            size:2,
            text:"<i>Le festival des jeux à Vichy </i><br><br> Clément & Roméo seront sur la partie pro !",
            type:"salon",
            highlight:false
          }
      ,
          {
            name:"La fête du jeu à Clermont L'Hérault",
            imageSource:"FeteDuJeu_ClermontHerault2022.jpg",
            link:"https://www.ville-clermont-herault.fr/culture-loisirs/1047-fete-du-jeu-2&Itemid=201",
            date:"le samedi 28 mai 2022",
            timestamp:1653729441000,
            size:2,
            text:"La fête du jeu à Clermont L'Hérault. <br>Nous ne pourrons pas être présents, mais une zone <b>Ma Première Aventure</b> installée ! ",
            type:"salon",
            highlight:false
          }
		  ,
			{
                name:"Sortie MPA8",
                imageSource:"../../GameOut/dahu/Book_3D_FR.png",
                link:"https://www.game-flow.fr/dahu",
                date:"25 août 2022",
				timestamp:1661418859000,
                size:2,
                text:"<br><i> Sortie de <b>Sur la Piste du Dahu</i></b> <br> 8ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },

            {
                    name:"OctoGônes",
                    imageSource:"Octogones2022.png",
                    link:"https://www.octogones.org/",
                    date:"du 30 septembre au 02 octobre",
                    timestamp:1664514018000,
                    size:2,
                    text:"<i>Le festival \"OctoGônes\" à Lyon </i><br><br>  Avec séance de dédicaces du jeu : Roméo et Zaël !",
                    type:"salon",
                    highlight:false
            },
            {
                      name:"Sortie A5E",
                      imageSource:"../../GameOut/5empereurs/Box_3D.png",
                      link:"https://www.game-flow.fr/5empereurs",
                      date:"30 septembre 2022",
					timestamp:1665205218000,
                      size:2,
                      text:"<br><i> Sortie de <b>L'Année des 5 Empereurs</i></b> <br> Soyez le seul et légitime Empereur de Rome ! ",
                      type:"empereurs",
                      highlight:false
            },
			{
					name:"SPIEL",
					imageSource:"SPIEL.jpg",
					link:"https://www.spiel-messe.com/en/",
					date:"du 06 au 09 octobre",
					timestamp:1665032418000,
					size:2,
					text:"<i>Le salon \"SPIEL\" à Essen </i><br><br>  Clément & Caroline y seront !",
					type:"essen",
					highlight:false
			},
			{
				  name:"25e Festival du jeu et du jouet",
				  imageSource:"FeteVosJeux.jpg",
				  link:"https://www.lessaisies.com/25eme-festival-du-jeu-et-jouet-distractions-et-loisirs-ugine-6211586.html",
				  date:"du 03 au 04 décembre",
				  timestamp:1670047218000,
				  size:2,
				  text:"<i>Le salon \"25e Festival du jeu et du jouet\" à Ugine. </i><br>  Avec un stand GameFlow!",
				  type:"salon",
				  highlight:false
			 },
             {
				name:"Sortie DDD",
				imageSource:"../../GameOut/Cyclone/Box_3D.png",
				link:"https://www.game-flow.fr/dorsdragondor",
				date:"18 novembre 2022",
				timestamp:1668762000000,
				size:2,
				text:"<br><i> Sortie de <b>Dors Dragon d'Or</i></b> <br> Ne réveillez pas le Dragon qui dort ! ",
				type:"DDD",
				highlight:false
			},
			{
                name:"Sortie MPA9",
                imageSource:"../../GameOut/bibliotheque/Book_3D_FR.png",
                link:"https://www.game-flow.fr/bibliotheque",
                date:"23 juin 2023",
				timestamp:1687507200000,
                size:2,
                text:"<br><i>Sortie de <b>La Bibliothèque Infinie</i></b><br> 9ème livre de la collection <br> Ma Première Aventure",
                type:"sortieMPA",
                highlight:false
            },
			{
				name:"PEL 2023",
				imageSource:"PEL2023.jpg",
				link:"https://www.parisestludique.fr/",
				date:"01 et 02 juillet 2023",
				timestamp:1688198400000,
				size:2,
				text:"<br>Paris Est Ludique à Paris <br> Avec des dédicaces de <b>Seppyo</b> et <b>Pauline</b> pour <b>La Bibliothèque Infinie</i></b>",
				type:"pel",
				highlight:false
          },

      {
        name:"Ludimania 2023",
        imageSource:"Ludimania2023.png",
        link:"https://ludimania.fr/",
        date:"09 et 10 septembre 2023",
        timestamp:1694783805000,
        size:2,
        text:"<br>Le salon de Ludimania. <br> Xavier, Clément et Arnaud Boutle y seront !</b>",
        type:"salon",
        highlight:false
          },

      {
            name:"Sortie Mon Puzzle Aventure",
            imageSource:"../../GameOut/puzzledragon/MPZ_Dragon_3D_Left_FR.png",
            link:"https://www.game-flow.fr/puzzledragon",
            date:"15 septembre 2023",
             timestamp:1695561405000,
            size:2,
            text:"<i>Sortie de <b>Mon Puzzle Aventure : Dragon</i></b><br> Le puzzle-jeu qui raconte des histoires !",
            type:"sortieMPZ",
            highlight:false
      } ,

      {
        name:"Vichy 2023",
        imageSource:"FJV23-Format-carre.png",
        link:"https://www.festivaldesjeuxvichy.fr/",
        date:"17 au 19 septembre 2023",
        timestamp:1696079805000,
        size:2,
        text:"<br>Le salon de Vichy <br> Nous serons sur la partie pro !</b>",
        type:"salon",
        highlight:false
      },

      {
        name:"Octogônes 2023",
        imageSource:"Octogones2023.png",
        link:"https://octogones.org/",
        date:"29 septembre au 01 octobre 2023",
        timestamp:1696511805000,
        size:2,
        text:"<br>Le salon d'OctoGônes <br> Xavier et Roméo y seront !</b>",
        type:"salon",
        highlight:false
          } ,

      {
            name:"SPIEL",
            imageSource:"SPIEL.jpg",
            link:"https://www.spiel-essen.de/en/",
            date:"du 5 au 8 octobre",
            timestamp:1697807805000,
            size:2,
            text:"<i>Le salon \"SPIEL\" à Essen </i><br><br>  Clément y sera !",
            type:"essen",
            highlight:false
       },

       {
             name:"Sortie MPA10",
             imageSource:"../../GameOut/argonuts/Book_3D_FR.png",
             link:"https://ma-premiere-aventure.fr/livres/pattie-lepreuve-des-dieux",
             date:"Le 27 octobre 2023",
 				     timestamp:1699193805000,
             size:2,
             text:"<i>Sortie de <b>Pattie et l'épreuve des dieux</i></b><br> 10ème livre de la collection <br> Ma Première Aventure",
             type:"sortieMPA",
             highlight:false
       },

       {
         name:"La foire de Grenoble",
         imageSource:"FoireGrenoble.png",
         link:"https://foiredegrenoble.com/",
         date:"3 au 4 novembre 2023",
         timestamp:1700056307000,
         size:2,
         text:"<br>La foire de Grenoble <br> nous y serons !</b>",
         type:"salon",
         highlight:false
           } ,

           {
             name:"Le festival du jeu et du jouet d'Ugine",
             imageSource:"Ugine.jpg",
             link:"https://www.ugine.com/",
             date:"2 au 3 décembre 2023",
             timestamp:1702648307000,
             size:2,
             text:"<br>Le festival du jeu et du jouet d'Ugine <br> nous y serons !</b>",
             type:"salon",
             highlight:false
               } ,

           {
               name:"FIJ 2024",
               imageSource:"affiche-FIJ-2024.jpg",
               link:"https://www.festivaldesjeux-cannes.com/fr/festival/all",
               date:"du 23 au 25 février",
               timestamp:1710073307000,
               size:2,
               text:" Nous serons présents au : <br> Festival international des jeux de Cannes !",
               type:"cannes",
               highlight:false
           },

     {
               name:"Place aux Jeux 2024",
               imageSource:"PlaceAuxJeux-2024.jpg",
               link:"http://placeauxjeux-grenoble.org/",
               date:"26 février au 3 mars 2024",
               timestamp:1710246107000 ,
               size:2,
               text:" Nous serons présents à : <br> Place aux jeux !",
               type:"grenoble",
               highlight:false
           },

     {
               name:"Festival à toi de jouer 2024",
               imageSource:"atoidejouer-2024.jpeg",
               link:"https://atoidejouer-dpa.fr/evenements",
               date:"9 au 10 mars 2024",
               timestamp:1710937307000 ,
               size:2,
               text:" Nous serons présents au : <br> Festival à toi de jouer !",
               type:"salon",
               highlight:false
           },

     {
               name:"Magnifique Livre 2024",
               imageSource:"magnifique_livre_2024.png",
               link:"https://magnifique-livre.org/",
               date:"16 au 17 mars 2024",
               timestamp:1711369307000 ,
               size:2,
               text:" Nous serons présents à : <br> Magnifique Livre !",
               type:"salon",
               highlight:false
           },

           {
                 name:"Sortie MPZOcre",
                 imageSource:"../../GameOut/puzzleocre/MPZ_Dragon_3D_Left_FR.png",
                 link:"https://www.mon-puzzle-aventure.fr/livres/mon-puzzle-aventure-terre-ocre",
                 date:"Le 30 août 2024",
                  timestamp:1725973681000,
                 size:2,
                 text:"<i>Sortie de <b>Mon Puzzle Aventure : Terre Ocre </i></b><br> 2ème puzzle de la collection <br> Mon Puzzle Aventure",
                 type:"sortieMPZ",
                 highlight:false
           },

           {
                 name:"Sortie MPA11",
                 imageSource:"../../GameOut/jungle/Book_3D_FR.png",
                 link:"https://ma-premiere-aventure.fr/livres/au-coeur-de-la-jungle",
                 date:"Le 20 septembre 2024",
                  timestamp:1727712132000,
                 size:2,
                 text:"<i>Sortie de <b>Au Coeur de la Jungle </i></b><br> 11ème livre de la collection <br> Ma Première Aventure",
                 type:"sortieMPA",
                 highlight:false
           },

		]
    }
};

const store = createStore(
    reducers,
    defaultStore,
    compose(
        applyMiddleware(...middlewares),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

const domElement = document.getElementById('main');

const history = syncHistoryWithStore(browserHistory, store);

const routing =
    <Provider store={store}>
        <RouterContainer history={history} store={store}/>
    </Provider>;

ReactDOM.render(routing, domElement);
