import {useRouterHistory} from 'react-router';
import {createHistory} from 'history';

export const apiUrls = {
    login: '/api/login',
    loginStorage: '/api/loginS',
    getSites: '/api/getSites',
    getAnalyseurs: '/api/getAnalyseurs/',
    getRoutesInfo: '/api/getRoutesInfo/',
    saveRoutesInfo: '/api/routesInfo/update',
    getAllAnalyses: '/api/getAllAnalyses',
    updateAnalyseur: '/api/analyseurs/update',
    chooseSiteStorage: '/api/chooseSiteS',
    exportAnalyses: '/api/exportAnalyses',
    exportPreana: '/api/preAna',
    exportAnalyseurs: '/api/exportAnalyseurs',
    exportQuery: '/api/exportQuery',
    exportLogs: '/api/exportLogs'
};

export const options = {
    accesPath: '../../../assets/image/Carroussel/smallImage/',
    bigImagePath: '../../../assets/image/Carroussel/bigImage/',
    carouselVisible: true
};


export const globalVariables = {
    carousel: {yearSize: 1832, clickStep:100, faidingDitance:75, seasonOffset:0}, // if my math where right the size should be yearSize: 1850 but you should play with that to change the time scale
    secondeInYear:31556926000,
    yearTextSeize:110,
    parcheSeize:200,
	scrollTop : 400
};

// Custom browser history avec prefix
export const browserHistory = useRouterHistory(createHistory)({
    basename: ''
});

