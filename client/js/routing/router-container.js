import {connect} from 'react-redux';
import {loadLanguage} from '../actions/login-actions';
import {bindActionCreators} from 'redux';
import Router from './router';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
		loadLanguage: bindActionCreators(loadLanguage, dispatch)
    };
};

const routerContainer = connect(mapStateToProps, mapDispatchToProps)(Router);
export default routerContainer;
