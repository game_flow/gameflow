import React, {Component, PropTypes} from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import {isEmpty} from 'lodash';
import Main from '../components/main/main-container.js';
import Reception from '../components/mainTarget/reception-container.js';
import GameOut from '../components/mainTarget/gameOut-container.js';
import GameComingComplicity from '../components/mainTarget/gameComing-Complicity-container.js';
import GameComingAffinityExt from '../components/mainTarget/gameComing-AffinitySensuelle-container.js';
import GameComingCyclone from '../components/mainTarget/gameComing-Cyclone-container.js';
import BookPage from '../components/mainTarget/bookPage-container.js';
import AtlantidePage from '../components/mainTarget/atlantidePage-container.js';
import Nous from '../components/mainTarget/nous-container.js';
import Contact from '../components/mainTarget/contact-container.js';
import Jeux from '../components/mainTarget/jeux-container.js';
import PhobosPage from '../components/mainTarget/phobosPage-container.js';
import OcrePage from '../components/mainTarget/ocrePage-container.js';
import RacePage from '../components/mainTarget/racePage-container.js';
import AbeillesPage from '../components/mainTarget/abeillesPage-container.js';
import enquetePage from '../components/mainTarget/enquetePage-container.js';
import dahuPage from '../components/mainTarget/dahuPage-container.js';
import bibliothequePage from '../components/mainTarget/bibliothequePage-container.js';
import argonutsPage from '../components/mainTarget/argonutsPage-container.js';
import junglePage from '../components/mainTarget/junglePage-container.js';
import puzzledragonPage from '../components/mainTarget/puzzledragonPage-container.js';
import puzzleocrePage from '../components/mainTarget/puzzleocrePage-container.js';
import puzzlephobosPage from '../components/mainTarget/puzzlephobosPage-container.js';
import GameComing5empereurs from '../components/mainTarget/gameComing-5empereurs-container.js';
import Page from '../components/mainTarget/dahuPage-container.js';
import SeriousGames from '../components/mainTarget/seriousGames-container.js';

export default class RouterContainer extends Component {

    constructor(props) {
        super(props);
        this.languageSet = false;
        this.routes = (
            <Router history={this.props.history}>
                <Route path="/" component={Main} onEnter={this.handleLanguage.bind(this)}>
                    <IndexRoute components={{main: undefined}}/>
                    <Route path="reception" components={{main: Reception, content: ""}}/>
                    <Route path="reception.fr"/>
                    <Route path="reception.en"/>
                    <Route path="jeux" components={{main: Jeux, content: ""}}/>
                    <Route path="jeux.fr"/>
                    <Route path="jeux.en"/>
				    <Route path="seriousGames" components={{main: SeriousGames, content: ""}}/>
                    <Route path="seriousGames.fr"/>
                    <Route path="seriousGames.en"/>
                    <Route path="chimere" components={{main: GameOut, content: ""}}/>
                    <Route path="chimere.fr"/>
                    <Route path="chimere.en"/>
                    <Route path="dorsdragondor" components={{main: GameComingCyclone, content: ""}}/>
                    <Route path="dorsdragondor.fr"/>
                    <Route path="dorsdragondor.en"/>
                    <Route path="affinity" components={{main: GameComingComplicity, content: ""}}/>
                    <Route path="affinity.fr"/>
                    <Route path="affinity.en"/>
					<Route path="affinityExtension" components={{main: GameComingAffinityExt, content: ""}}/>
                    <Route path="affinityExtension.fr"/>
                    <Route path="affinityExtension.en"/>
                    <Route path="premiereAventure" components={{main: BookPage, content: ""}}/>
                    <Route path="premiereAventure.fr"/>
                    <Route path="premiereAventure.en"/>
                    <Route path="atlantide" components={{main: AtlantidePage, content: ""}}/>
                    <Route path="atlantide.fr"/>
                    <Route path="atlantide.en"/>
					<Route path="phobos" components={{main: PhobosPage, content: ""}}/>
                    <Route path="phobos.fr"/>
                    <Route path="phobos.en"/>
					<Route path="ocre" components={{main: OcrePage, content: ""}}/>
                    <Route path="ocre.fr"/>
                    <Route path="ocre.en"/>
					<Route path="cassetout" components={{main: RacePage, content: ""}}/>
                    <Route path="cassetout.fr"/>
                    <Route path="cassetout.en"/>
          <Route path="champfleuri" components={{main: AbeillesPage, content: ""}}/>
                    <Route path="champfleuri.fr"/>
                    <Route path="champfleuri.en"/>
          <Route path="auvoloeuf" components={{main: enquetePage, content: ""}}/>
                     <Route path="auvoloeuf.fr"/>
                     <Route path="auvoloeuf.en"/>
         <Route path="dahu" components={{main: dahuPage, content: ""}}/>
                     <Route path="dahu.fr"/>
                     <Route path="dahu.en"/>
					<Route path="bibliotheque" components={{main: bibliothequePage, content: ""}}/>
                    <Route path="bibliotheque.fr"/>
                    <Route path="bibliotheque.en"/>
          <Route path="argonuts" components={{main: argonutsPage, content: ""}}/>
                    <Route path="argonuts.fr"/>
                    <Route path="argonuts.en"/>
          <Route path="jungle" components={{main: junglePage, content: ""}}/>
                    <Route path="jungle.fr"/>
                    <Route path="jungle.en"/>

          <Route path="puzzledragon" components={{main: puzzledragonPage, content: ""}}/>
                    <Route path="puzzledragon.fr"/>
                    <Route path="puzzledragon.en"/>

          <Route path="puzzleocre" components={{main: puzzleocrePage, content: ""}}/>
                    <Route path="puzzleocre.fr"/>
                    <Route path="puzzleocre.en"/>

          <Route path="puzzlephobos" components={{main: puzzlephobosPage, content: ""}}/>
                    <Route path="puzzlephobos.fr"/>
                    <Route path="puzzlephobos.en"/>

        <Route path="5empereurs" components={{main: GameComing5empereurs, content: ""}}/>
                     <Route path="5empereurs.fr"/>
                     <Route path="5empereurs.en"/>
          <Route path="nous" components={{main: Nous, content: ""}}/>
                    <Route path="nous.fr"/>
                    <Route path="nous.en"/>
          <Route path="contact" components={{main: Contact, content: ""}}/>
                    <Route path="contact.fr"/>
                    <Route path="contact.en"/>
                </Route>
            </Router>
        );
    }

    handleLanguage(nextState, replace) { //rederecting the URL with langage to the cleaned URL plus loading language
        let directionUrl = nextState.location.pathname;
        let tempCutUrl = directionUrl.split('.');
        if (tempCutUrl[tempCutUrl.length - 1].toLowerCase() === "en" || tempCutUrl[tempCutUrl.length - 1].toLowerCase() === "fr") {
            this.props.loadLanguage(tempCutUrl[tempCutUrl.length - 1].toUpperCase());
            this.languageSet = true;
            directionUrl = nextState.location.pathname.split('.')[0];
        } else if (!this.languageSet) {
            this.props.loadLanguage("FR"); //this is the default languge
            this.languageSet = true;
        }
        if (directionUrl === '/') {
            replace({
                pathname: '/reception'
            });
        } else if (nextState.location.pathname !== directionUrl) {
            replace({
                pathname: directionUrl
            });
        }
    }

    render() {
        return this.routes;
    }
}

RouterContainer.propTypes = {
    history: PropTypes.object.isRequired
};
