import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import FullscreenPopup from './fullscreenPopup';
import {hiddePopup} from '../actions/main-actions';

const mapStateToProps = (state) => {
    return {
        language: state.language,
        popupInfo : state.main.popupInfo,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hiddePopup: bindActionCreators(hiddePopup, dispatch)
    };
};

const fullscreenPopupContainer = connect(mapStateToProps, mapDispatchToProps)(FullscreenPopup);
export default fullscreenPopupContainer;
