import React, {Component, PropTypes} from 'react';



export default class FullscreenPopup extends Component {
    constructor(props) {
        super(props);

    }

    handleClick(){
        this.props.hiddePopup();
    }

	changeLanguage( langue ){
		this.props.loadLanguage(language[langue]);
	}

    loadLink(){
        window.open(this.props.popupInfo.item.link)
    }
	
    render() {
	    const popupId =  this.props.popupInfo.isVisible? "fullscreenPopupVisible" : "fullscreenPopupHidden";
		return(
            <div id={popupId} onClick={this.handleClick.bind(this)}>
                { this.props.popupInfo.item.hasOwnProperty('link')?
                    <div className="linkContainer"  onClick={this.loadLink.bind(this)}>
                        <div className={"carousel-icone-link-fullScreen"}/>
                        <div className="linkLabel">{this.props.language["linkText"]}</div>
                    </div> : ""
                }
                <div  id="greyBackground" onClick={this.handleClick.bind(this)}></div>
				<div  id="poster"  style={{backgroundImage: "url(" + this.props.popupInfo.posterImagePath + ")"}}>
				</div>
            </div>
		)
	}
}

FullscreenPopup.propTypes = {
    language: PropTypes.object,
    lab: PropTypes.string,
    openNotification: PropTypes.func,
    hasNotification: PropTypes.bool
};
