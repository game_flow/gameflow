import {findIndex, concat, difference, sortBy} from 'lodash';
import {LOAD_LANGUAGE} from '../actions/login-actions';

const languageReducer = (state = {}, action) => {
    switch (action.type) {
        case LOAD_LANGUAGE : {
            let newState = {...state};
            newState = action.language;
            return newState;
        }

        default:
            return state;
    }
};

export default function (state = {}, action) {
    return languageReducer(state, action);
}
