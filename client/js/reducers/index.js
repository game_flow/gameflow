import {combineReducers} from "redux";

import languageReducer from './language-reducer';
import routerReducer from './router-reducer';
import mainReducer from './main-reducer';

export default combineReducers({
    language: languageReducer,
    routing: routerReducer,
    main: mainReducer,
});
