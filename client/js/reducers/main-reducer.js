import {findIndex, concat, difference, sortBy} from 'lodash';
import {UPDATE_BANNER_CLASS, UPDATE_POSTER, HIDDE_POPU} from '../actions/main-actions';

const languageReducer = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_BANNER_CLASS : {
            let newState = {...state};
            newState.bannerClass = action.bannerClass;
            return newState;
        }
        case UPDATE_POSTER : {
            let newState = {...state};
            newState.popupInfo.item = action.item;
            newState.popupInfo.posterImagePath = action.imagePath;
            newState.popupInfo.isVisible = action.isVisible;
            newState.popupInfo = {...newState.popupInfo};

          return newState;
        }
        case HIDDE_POPU : {
            let newState = {...state};
            newState.popupInfo.isVisible = false;
            newState.popupInfo = {...newState.popupInfo};
            newState.refreshItems = !newState.refreshItems;
            return newState;
        }



        default:
            return state;
    }
};

export default function (state = {}, action) {
    return languageReducer(state, action);
}
