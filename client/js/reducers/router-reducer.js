import {LOCATION_CHANGE} from 'react-router-redux';


// This initial state is *copied* from react-router-redux's
// routerReducer (the property name 'locationBeforeTransitions' is
// because this is designed for use with react-router)
const initialState = {locationBeforeTransitions: null};

const routerReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOCATION_CHANGE : {
            return {...state, locationBeforeTransitions: action.payload}
        }

        default:
            return state
    }
};

export default function (state = {}, action) {
    return routerReducer(state, action);
}
